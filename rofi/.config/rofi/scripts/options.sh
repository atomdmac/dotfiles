#!/bin/bash

# List options
if [ -z $@ ]
then
  echo -e "one\ntwo\nthree"
elif [ $1 = "three" ]
then
  urxvt >/dev/null &
  exit
fi

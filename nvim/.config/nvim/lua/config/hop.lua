local map = require("utils").map
local hop = require("hop")
hop.setup({
  multi_windows = true
})

map({ "n", "s", "<cmd>HopChar1<CR>" })
map({ "v", "s", "<cmd>HopChar1<CR>" })
map({ "o", "gs", "<cmd>HopChar1<CR>" })

local map = require("utils").map
local whichkey = require("which-key")

vim.g.lcov_coverage_files = {
  "lcov.info",
  "coverage.lcov",
  "coverage/lcov.info",
  "coverage/jest/lcov.info",
}

map({ "n", "<leader>cc", ":LcovShow<CR>" })
map({ "n", "<leader>ch", ":LcovClear<CR>" })

whichkey.register({
  c = {
    c = { "Show Code Coverage" },
    h = { "hide Code Coverage" },
  },
}, { prefix = "<leader>" })

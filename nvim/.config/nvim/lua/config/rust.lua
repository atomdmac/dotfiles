-- Rust-specific development stuff
local map = require("utils").map
local Terminal = require("toggleterm.terminal").Terminal
local rust_terminal = Terminal:new({ direction = "horizontal", hidden = false })

local M = {}

M.open_rust_terminal = function()
  -- TODO: Disable when not in a Rust project
  -- TODO: Convert to pure Lua

  -- Use TermToggle plugin if available
  if vim.fn.exists(":TermExec") then
    if rust_terminal:is_open() == false then
      rust_terminal:toggle()
    end
    rust_terminal:send("cargo run", true)
  else
    vim.cmd([[
      :sp
      :terminal cargo run
      :nnoremap <buffer> q :q<CR>
      ]])
  end
end

-- map({"n", "<space>rr", ":sp<CR>:terminal cargo run<CR>", noremap = true})
map({
  "n",
  "<space>rr",
  ":lua require('config/rust').open_rust_terminal()<CR>",
  noremap = true,
})

return M

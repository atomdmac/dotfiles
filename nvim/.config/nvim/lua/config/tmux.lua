-- Set keybinds to forward to tmux nav
vim.keymap.set('n', '<M-h>', require'nvim-tmux-navigation'.NvimTmuxNavigateLeft)
vim.keymap.set('n', '<M-j>', require'nvim-tmux-navigation'.NvimTmuxNavigateDown)
vim.keymap.set('n', '<M-k>', require'nvim-tmux-navigation'.NvimTmuxNavigateUp)
vim.keymap.set('n', '<M-l>', require'nvim-tmux-navigation'.NvimTmuxNavigateRight)
vim.keymap.set('n', '<M-\\>', require'nvim-tmux-navigation'.NvimTmuxNavigateLastActive)
vim.keymap.set('n', '<M-Space>', require'nvim-tmux-navigation'.NvimTmuxNavigateNext)

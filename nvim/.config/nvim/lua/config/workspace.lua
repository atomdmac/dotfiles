local map = require("utils").map
local whichkey = require("which-key")

vim.g.workspace_session_name = ".session.vim"
vim.g.workspace_session_disable_on_args = true
vim.g.workspace_autosave_ignore = {
  "gitcommit",
  "scratch",
  "pad",
  "qf",
  "help",
  "vim-plug",
}
vim.g.workspace_autosave_always = false
vim.g.workspace_autosave = false
vim.cmd([[
  let g:workspace_session_directory = $HOME . "/.vim/sessions"
  let g:workspace_undodir = &undodir
]])

-- Normal Mode
map({ "n", "<leader>ws", ":ToggleWorkspace<CR>" })
whichkey.register({
  w = {
    s = {
      "Toggle Workspace",
    },
  },
}, { prefix = "<leader>", mode = "n" })

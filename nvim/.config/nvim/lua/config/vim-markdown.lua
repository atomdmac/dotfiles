-- The way this plugin interacts with the `foldlevel` setting does some weird
-- things (like collapse headings that the cursor isn't under).  In theory this
-- would be nice but it's really annoying to me so I'm just disabling the whole
-- thing for now.
vim.g.vim_markdown_folding_disabled = true

vim.g.vim_markdown_conceal = true
vim.g.vim_markdown_new_list_item_indent = false
vim.g.conceallevel = 2

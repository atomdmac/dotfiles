local map = require("utils").map
local whichkey = require("which-key")

require("toggleterm").setup({
  -- size can be a number or function which is passed the current terminal
  size = function(term)
    if term.direction == "horizontal" then
      return 15
    elseif term.direction == "vertical" then
      return vim.o.columns * 0.4
    end
  end,
  -- open_mapping = [[<c-\>]],
  -- on_open = fun(t: Terminal), -- function to run when the terminal opens
  -- on_close = fun(t: Terminal), -- function to run when the terminal closes
  hide_numbers = true, -- hide the number column in toggleterm buffers
  shade_filetypes = {},
  shade_terminals = true,
  -- shading_factor = "<number>", -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
  start_in_insert = true,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
  persist_size = true,
  direction = "vertical", -- | "horizontal" | "window" | "float",
  close_on_exit = true, -- close the terminal window when the process exits
  shell = vim.o.shell, -- change the default shell
  -- This field is only relevant if direction is set to 'float'
  float_opts = {
    -- The border key is *almost* the same as 'nvim_open_win'
    -- see :h nvim_open_win for details on borders however
    -- the 'curved' border is a custom border type
    -- not natively supported but implemented in this plugin.
    border = "double", -- single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
    -- width = <value>,
    -- height = <value>,
    winblend = 3,
    highlights = { border = "Normal", background = "Normal" },
  },
})

-- Normal Mode
map({
  "n",
  "<space>tt",
  "<Cmd>exe v:count1 . 'ToggleTerm direction=vertical'<CR>",
})
map({
  "n",
  "<space>tj",
  "<Cmd>exe v:count1 . 'ToggleTerm direction=horizontal'<CR>",
})
map({ "n", "<space>ta", ":ToggleTermToggleAll<CR>" })

whichkey.register({
  t = {
    name = "Terminal",
    t = { "Open Vertical" },
    l = { "Open VSplit (Right)" },
    j = { "Open Split (Bottom)" },
  },
}, { prefix = "<space>" })


-- Unmap the existing keymaps from .vimrc to eliminate dalay
vim.cmd("nunmap <a-t>")

-- Insert Mode
map({ "i", "<a-t>", "<Cmd>exe v:count1 . 'ToggleTerm'<CR>" }, { silent = true, noremap = true })
map({ "n", "<a-t>", "<Cmd>exe v:count1 . 'ToggleTerm'<CR>" }, { silent = true, noremap = true })

-- Terminal Mode
map(
  { "t", "<a-t>", "<Cmd>exe v:count1 . 'ToggleTerm'<CR>" },
  { noremap = true }
)

require("nvim-autopairs").setup({
  fast_wrap = {},
  check_ts = true,
})

-- Custom colors for displaying matching parents/braces/etc.
vim.cmd([[
:hi MatchParen ctermbg=black guibg=black cterm=underline gui=underline
:hi MatchParenCur cterm=underline gui=underline
:hi MatchWordCur cterm=underline gui=underline
]])

local map = require("utils").map

vim.g.vimwiki_list = {
  {
    path = "~/Seafile/default/Documents/vimwiki/",
    syntax = "markdown",
    ext = ".md",
  },
  {
    path = "~/Seafile/default/Documents/rpg-wiki/",
    syntax = "markdown",
    ext = ".md",
  },
}

vim.g.vimwiki_folding = "custom"
vim.g.vimwiki_global_ext = 1

-- Disable some vimwiki keybinds that clash with my other keybinds.
vim.g.vimwiki_key_mappings = { lists = 0 }

-- " General/Global keybinds
-- nmap <leader>wl :VimwikiUISelect<CR>
map({ "n", "<leader>wl", ":VimwikiUISelect<CR>" })

local M = {}

-- function SetVimWikiKeybinds ()
M.SetVimWikiKeybinds = function()
  map({ "n", "<leader>wt", ":VimwikiToggleListItem<CR>" }, { buffer = true })
  vim.bo.syntax = "markdown"
end
vim.cmd(
  [[autocmd FileType vimwiki lua require('config/vimwiki').SetVimWikiKeybinds()]]
)

M.SetMarkdownFolding = function()
  vim.wo.foldmethod = "expr"
  -- Attempting to set foldexpr to a vim.cmd(...) function was not working so
  -- I'm just wrapping the whole dang setting in a vim.cmd(...).  It works-ish.
  vim.cmd([[
    setlocal foldexpr=NestedMarkdownFolds()
  ]])
end
vim.cmd(
  [[autocmd Syntax markdown lua require('config/vimwiki').SetMarkdownFolding()]]
)

return M

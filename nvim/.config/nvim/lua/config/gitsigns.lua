local map = require("utils").map
local whichkey = require("which-key")

require("gitsigns").setup({
  on_attach = function()
    map({
      "n",
      "]c",
      "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'",
      expr = true,
    })
    map({
      "n",
      "[c",
      "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'",
      expr = true,
    })
    map({
      "n",
      "<space>gb",
      ":Gitsigns blame_line<CR>",
    }, {
      silent = true,
    })
    whichkey.register({
      ["["] = {
        c = {
          "Goto Previous Unstaged Change",
        },
      },

      ["]"] = {
        c = {
          "Goto Next Unstaged Change",
        },
      },
    }, { mode = "n" })
  end,
})

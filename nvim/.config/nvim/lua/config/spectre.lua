local map = require("utils").map
local spectre = require("spectre")
local whichkey = require("which-key")

spectre.setup()

-- Normal Mode
map({ "n", "<space>sp", ":lua require('spectre').open()<CR>i" })
whichkey.register({
  s = {
    p = { "Search Project" },
  },
}, { prefix = "<space>", mode = "n" })

-- Visual Mode
map({ "v", "<space>sp", ":lua require('spectre').open_visual()<CR>" })
whichkey.register({
  s = {
    p = { "Search Project" },
  },
}, { prefix = "<space>", mode = "v" })

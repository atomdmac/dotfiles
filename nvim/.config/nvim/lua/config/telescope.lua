local utils = require("utils")
local map = utils.map
local telescope = require("telescope")
local actions = require("telescope.actions")
local whichkey = require("which-key")

local M = {}

M.telescope_project_files = function()
  local opts = {} -- define here if you want to define something
  local ok = pcall(require("telescope.builtin").git_files, opts)
  if not ok then
    require("telescope.builtin").find_files(opts)
  end
end

-- Search for files
map({
  "n",
  "<space><space>",
  "<cmd>lua require('config/telescope').telescope_project_files()<CR>",
})
map({ "n", "<space>fa", ":Telescope find_files<CR>" })
map({ "n", "<space>fb", ":Telescope buffers<CR>" })
map({ "n", "<space>fm", ":Telescope marks<CR>" })
map({ "n", "<space>fe", ":Telescope file_browser<CR>" })
vim.keymap.set("n", "<space>fe", function()
  require("telescope").extensions.file_browser.file_browser({
    path = "%:p:h",
  })
end)
-- Search Git
map({ "n", "<space>gc", ":Telescope git_branches<CR>" })
map({ "n", "<space>gs", ":Telescope git_stash<CR>" })
-- Search for file contents
map({ "n", "<space>ss", ":Telescope live_grep<CR>" })
map({ "n", "<space>sb", ":Telescope current_buffer_fuzzy_find<CR>" })
-- Search for terminals
map({ "n", "<space>ft", ":Telescope termfinder<CR>" })
-- GitHub Integration
map({ "n", "<space>ghp", ":Telescope gh pull_request<CR>" })
map({ "n", "<space>ghr", ":Telescope gh run<CR>" })

whichkey.register({
  f = {
    name = "Find / Select",
    a = { "File" },
    b = { "Buffer" },
    m = { "Marks" },
    t = { "Terminal" },
  },
  g = {
    c = { "Branch" },
    s = { "Stash" },
    h = {
      name = "Github",
      p = { "Pull Request" },
      r = { "Run" },
    },
  },
  s = {
    name = "Search for Text",
    s = { "...in Project" },
    b = { "...in Buffer" },
  },
}, { prefix = "<space>" })

local themes = {
  vertical_full_screen = {
    layout_strategy = "vertical",
    layout_config = { preview_height = 0.80, height = 1000, width = 1000 },
  },
}

telescope.setup({
  defaults = {
    prompt_prefix = "🔍 ",
    mappings = { i = { ["<esc>"] = actions.close } },
    vimgrep_arguments = {
      "rg",
      "--sortr=modified",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
    },
    additional_args = {
      "--hidden",
    },
  },
  pickers = {
    git_files = { theme = "dropdown" },
    find_files = { theme = "dropdown", hidden = true },
    buffers = { theme = "dropdown" },
    git_stash = themes.vertical_full_screen,
    git_branches = themes.vertical_full_screen,
    live_grep = {
      layout_strategy = "vertical",
      layout_config = { preview_height = 0.80, height = 1000, width = 1000 },
      additional_args = function()
        return { "--hidden", "--iglob", "!.git" }
      end,
    },
    current_buffer_fuzzy_find = { theme = "dropdown" },
    marks = { theme = "dropdown" },
  },
  extensions = { file_browser = { respect_gitignore = false, hidden = true } },
})

telescope.load_extension("fzf")
telescope.load_extension("file_browser")
telescope.load_extension("termfinder")
telescope.load_extension("gh")

return M

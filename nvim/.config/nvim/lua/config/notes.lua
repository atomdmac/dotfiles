local map = require("utils").map
local whichkey = require("which-key")

-- vim.g.notes_directory = "~/Seafile/default/Documents/vim-pad/"
vim.g.notes_directory = "~/Documents/Notes/"
vim.g.notes_auto_rename = 1

-- Normal Mode
local opts = { silent = true, noremap = true }
map({ "n", "<space>nn", ":NotesCreate<CR>" }, opts)
map({
  "n",
  "<space>nl",
  -- ":Telescope live_grep search_dirs=['~/Seafile/default/Documents/vim-pad/']<CR>",
  -- TODO: Better formatting for notes search command
  -- TODO: Only sort results based on `modified` for notes.  All others should be best match only.
  ":lua require('telescope.builtin').live_grep({search_dirs = {'~/Seafile/default/Documents/vim-pad/'}})<CR>",
}, opts)
map({ "n", "<space>nL", ":NotesList<CR>" }, opts)
map({ "n", "<space>ns", ":NotesSearch<CR>" }, opts)
map({ "n", "<space>nm", ":NotesManage<CR>" }, opts)

whichkey.register({
  n = {
    name = "Notes",
    n = "New Note",
    l = "Search Notes",
    L = "List Notes",
    m = "Manage Notes",
    s = "Scratch Pad",
  },
}, { prefix = "<space>" })

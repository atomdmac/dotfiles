require("notify").setup({
  stages = "static"
})
-- Use nvim-notify as default notifier
vim.notify = require("notify")
-- Dismiss notifications
vim.keymap.set("n", "<leader>nd", function () require("notify").dismiss() end)
-- Show notification history
vim.keymap.set("n", "<leader>nh", ":Notifications<CR>")

vim.keymap.set("n", "<space>ww", function ()
  vim.cmd([[:w]])
  vim.notify('File Saved', "info", { title = '!! INFO !!', timeout = 2000 })
end)

local map = require("utils").map
local whichkey = require("which-key")

-- Normal Mode
map({ "n", "<space>ev", ":Vifm<CR>", noremap = true })
whichkey.register({
  e = {
    v = {
      "...Using ViFM",
    },
  },
}, { prefix = "<space>" })

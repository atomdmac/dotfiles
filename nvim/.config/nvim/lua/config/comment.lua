local whichkey = require("which-key")
local map = require("utils").map

-- Integrate with the ts-context-commentstring plugin for TreeSitter
local ts_context_commentstring_hook = function(ctx)
  local U = require("Comment.utils")

  local location = nil
  if ctx.ctype == U.ctype.block then
    location = require("ts_context_commentstring.utils").get_cursor_location()
  elseif ctx.cmotion == U.cmotion.v or ctx.cmotion == U.cmotion.V then
    location =
      require("ts_context_commentstring.utils").get_visual_start_location()
  end

  return require("ts_context_commentstring.internal").calculate_commentstring({
    key = ctx.ctype == U.ctype.line and "__default" or "__multiline",
    location = location,
  })
end

require("Comment").setup({
  mappings = {
    basic = false,
    extra = false,
    extended = false,
  },
  pre_hook = ts_context_commentstring_hook,
})

local opt = { expr = true, noremap = false }

-- Toggle using count
map(
  {
    "n",
    "<space>cc",
    "v:count == 0 ? '<Plug>(comment_toggle_current_linewise)' : '<Plug>(comment_toggle_linewise_count)'",
  },
  opt
)
map(
  {
    "n",
    "<space>cb",
    "v:count == 0 ? '<Plug>(comment_toggle_current_blockwise)' : '<Plug>(comment_toggle_blockwise_count)'",
  },
  opt
)

-- Toggle in Op-pending mode
map({ "n", "gc", "<Plug>(comment_toggle_linewise)" })
map({ "n", "gb", "<Plug>(comment_toggle_blockwise)" })

-- Toggle in VISUAL mode
map({ "x", "<space>cc", "<Plug>(comment_toggle_linewise_visual)" })
map({ "x", "<space>cb", "<Plug>(comment_toggle_blockwise_visual)" })

whichkey.register({
  c = {
    name = "Comments",
    c = "Toggle Line-Style Comment",
    b = "Toggle Block-Style Comment",
  },
}, { prefix = "" })

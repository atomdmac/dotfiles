local map = require("utils").map

-- Normal Mode
map({ "n", "`", ":Pounce<CR>", silent = true })
map({ "n", "s", ":Pounce<CR>", silent = true })
map({ "n", "S", ":PounceRepeat<CR>", silent = true })
map({ "v", "s", "<cmd>Pounce<CR>", silent = true })
map({ "o", "z", "<cmd>Pounce<CR>", silent = true })

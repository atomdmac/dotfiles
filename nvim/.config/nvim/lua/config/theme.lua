-- Color scheme options
vim.g.edge_style = "neon"
vim.g.edge_disable_italic_comment = true

-- If you have vim >=8.0 or Neovim >= 0.1.5
vim.g.termguicolors = 1
if vim.fn.has("termguicolors") == 1 and vim.g.termguicolors == 1 then
  vim.cmd("set termguicolors")
end

-- Theme
vim.cmd("colorscheme dracula")

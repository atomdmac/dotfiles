local map = require("utils").map
local whichkey = require("which-key")

vim.g.ranger_map_keys = false

-- Normal Mode
map({ "n", "<space>ee", ":Ranger<CR>", noremap = true })
whichkey.register({
  e = {
    name = "Explore Files",
    e = {
      "...Using Ranger",
    },
    v = {
      "...Using ViFM",
    },
  },
}, { prefix = "<space>" })

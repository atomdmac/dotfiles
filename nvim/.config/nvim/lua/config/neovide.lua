vim.g.guifont = "Hasklug NF"
if vim.fn.exists("g:neovide") == 1 then
  print("Neovide in Effect!")
  vim.g.neovide_cursor_vfx_mode = "sonicboom"
  vim.g.neovide_refresh_rate = 140
end

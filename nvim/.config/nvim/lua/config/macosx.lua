-- General settings specific to when running in Mac OSX

if vim.fn.has("macunix") == 1 then
  -- Explicitly set python paths for OSX
  vim.g.python_host_prog = "~/.pyenv/shims/python"
  vim.g.python3_host_prog = "~/.pyenv/shims/python3"
end

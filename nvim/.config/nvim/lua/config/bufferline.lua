local map = require("utils").map
local whichkey = require("which-key")

require("bufferline").setup({
  options = {
    show_buffer_close_icons = false,
    show_close_icon = false,
    diagnostics = "nvim_lsp",
    separator_style = { "<", "" },
  },
})

map({ "n", "<space>bb", ":BufferLinePick<CR>" }, { silent = true })
whichkey.register({
  b = {
    b = { "Select Buffer" },
  },
}, { prefix = "<space>" })

-------------------------------------------------------------------------------
-- SQL Language Server
-------------------------------------------------------------------------------
local nvim_lsp = require("lspconfig")
local helpers = require("config/lsp/helpers")

nvim_lsp["sqlls"].setup({
  cmd = { "sql-language-server", "up", "--method", "stdio" },
  filetypes = { "sql", "mysql" },
  on_attach = helpers.on_attach_no_formatting,
  flags = helpers.get_flags(),
  settings = {
    lint = {
      rules = {
        ["align-column-to-the-first"] = "error",
        ["column-new-line"] = "error",
        ["linebreak-after-clause-keyword"] = "off",
        ["reserved-word-case"] = { "error", "upper" },
        ["space-surrounding-operators"] = "error",
        ["where-clause-new-line"] = "error",
        ["align-where-clause-to-the-first"] = "error",
      },
    },
  },
})

-------------------------------------------------------------------------------
-- Golang LSP Setup
-------------------------------------------------------------------------------
local nvim_lsp = require("lspconfig")
local helpers = require("config/lsp/helpers")

nvim_lsp["gopls"].setup({
  init_options = { documentFormatting = false },
  on_attach = helpers.on_attach,
  flags = helpers.get_flags(),
})


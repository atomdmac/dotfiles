local whichkey = require("which-key")
local map = require("utils").map

local M = {}

M.on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

  -- Set autocommands conditional on server_capabilities
  if client.server_capabilities.document_highlight then
    vim.api.nvim_exec(
      [[
      set updatetime=500
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]],
      false
    )
  end

  -- Key Mappings.
  local opts = { buffer = bufnr, noremap = true, silent = true }

  map({ "n", "<space>lt", "<cmd>TroubleToggle<CR>" }, opts)
  map(
    { "n", "<space>lE", "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>" },
    opts
  )
  map({
    "n",
    "<space>le",
    "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>",
  }, opts)
  map({ "n", "<space>lh", "<cmd>lua vim.lsp.buf.hover()<CR>" }, opts)
  map({ "n", "<leader>ff", "<cmd>lua vim.lsp.buf.formatting()<CR>" }, opts)
  map({
    "n",
    "<leader>ft",
    "<cmd>lua require('config/lsp/formatting').toggle_formatting()<CR>",
  }, opts)
  map({
    "n",
    "<leader>fs",
    "<cmd>lua require('config/lsp/formatting').toggle_formatting_on_save()<CR>",
  }, opts)
  map({ "n", "[e", "<cmd>lua vim.diagnostic.goto_prev()<CR>" }, opts)
  map({ "n", "]e", "<cmd>lua vim.diagnostic.goto_next()<CR>" }, opts)
  map({ "n", "<space>ld", "<cmd>lua vim.lsp.buf.definition()<CR>" }, opts)
  map({ "n", "<space>lr", "<cmd>lua vim.lsp.buf.rename()<CR>" }, opts)
  map({ "n", "<space>ll", "<cmd>lua vim.lsp.buf.references()<CR>" }, opts)
  map({ "n", "<space>la", "<cmd>lua vim.lsp.buf.code_action()<CR>" }, opts)
  map({ "n", "<space>ls", "<cmd>lua vim.lsp.buf.signature_help()<CR>" }, opts)
  map({ "n", "<space>gi", "<cmd>lua vim.lsp.buf.implementation()<CR>" }, opts)
  map({
    "n",
    "<space>lD",
    "<cmd>lua require('diaglist').open_all_diagnostics()<CR>",
  }, opts)

  -- Document keymaps via which-key
  whichkey.register({
    ["["] = {
      e = {
        "Go to Previous Error",
      },
    },
    ["]"] = {
      e = { "Go to Next Error" },
    },
    ["<leader>"] = {
      f = {
        name = "Formatting",
        f = { "Format Document" },
        t = { "Toggle Formatting" },
        s = { "Toggle Formatting on Save" },
      },
    },
    ["<space>"] = {
      l = {
        name = "LSP Tools",
        a = { "Code Actions" },
        h = { "Hover/Help" },
        D = { "Open All Diagnostics" },
        r = { "Rename Symbol" },
        s = { "Signature Help" },
        l = { "List References" },
        E = {
          "Show Document Errors",
        },
        e = {
          "Show Error Here",
        },
        d = { "Go to Definition" },
      },
    },
  }, opts)
end

-- Disable formatting for client on attach
M.on_attach_no_formatting = function(client, bufnr)
  -- Handle formatting with null-ls
  -- Older version of the setting
  client.server_capabilities.document_formatting = false
  -- Newer version of the setting
  client.server_capabilities.documentFormattingProvider = false

  M.on_attach(client, bufnr)
end

M.get_flags = function()
  return { debounce_text_changes = 150 }
end

return M

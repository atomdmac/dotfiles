-------------------------------------------------------------------------------
-- Null-LS (Formatting / Linting)
-------------------------------------------------------------------------------
require("config/lsp/formatting")
local helpers = require("config/lsp/helpers")
local null_ls = require("null-ls")

null_ls.setup({
  on_attach = helpers.on_attach,
  sources = {
    null_ls.builtins.formatting.prettier.with({
      filetypes = {
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact",
        "html",
        "json",
        "tsx",
        "jsx",
        "css",
      },
      condition = function(utils)
        vim.g.formatting_enabled = utils.root_has_file({
          ".prettierrc.json",
          ".prettierrc.js",
          ".prettierrc",
        })
        return true
      end,
      runtime_condition = function()
        if vim.g.formatting_enabled ~= nil then
          return vim.g.formatting_enabled
        else
          return false
        end
      end,
      prefer_local = "node_modules/.bin",
    }),
    null_ls.builtins.formatting.stylua,
    null_ls.builtins.formatting.rustfmt,
    null_ls.builtins.diagnostics.luacheck.with({
      args = {
        "--globals",
        "vim",
        "--formatter",
        "plain",
        "--codes",
        "--ranges",
        "--filename",
        "$FILENAME",
        "-",
      },
    }),
    null_ls.builtins.diagnostics.eslint_d,
    null_ls.builtins.diagnostics.shellcheck.with({
      diagnostics_format = "[#{c}] #{m} (#{s})",
      filetypes = { "sh", "bash" }
    }),
  },
})

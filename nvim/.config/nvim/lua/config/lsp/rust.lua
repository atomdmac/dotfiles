local nvim_lsp = require("lspconfig")
local helpers = require("config/lsp/helpers")

-------------------------------------------------------------------------------
-- Rust Tools
-- Special setup to allow rust-tools and rust_analyzer to play together.
-------------------------------------------------------------------------------
require("rust-tools").setup({
  server = vim.tbl_deep_extend(
    "force",
    nvim_lsp["rust_analyzer"].document_config.default_config,
    {
      on_attach = helpers.on_attach_no_formatting,
      flags = helpers.get_flags(),
    }
  ),
})

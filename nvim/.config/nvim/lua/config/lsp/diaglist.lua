-------------------------------------------------------------------------------
-- Live-reload diagnostics in QuickFix window
-------------------------------------------------------------------------------
require("diaglist").init({
  -- optional settings
  -- below are defaults
  debug = false,
  -- increase for noisy servers
  debounce_ms = 150,
})

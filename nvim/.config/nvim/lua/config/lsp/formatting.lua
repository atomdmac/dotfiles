-------------------------------------------------------------------------------
-- General Formatting Utilities
-------------------------------------------------------------------------------

local M = {}

-- Format on save using LSP-based formatting.
if vim.g.formatting_on_save == nil then
  vim.g.formatting_on_save = true
end

-- Global override for formatting
vim.g.formatting_enabled = false

vim.api.nvim_exec(
  [[
  function DoFormatWrite()
    if g:formatting_on_save == 1 && g:formatting_enabled == 1
      " Formatting must be synchronous to avoid issue where the buffer is
      " formatted but not saved to disk.
      :lockmarks lua vim.lsp.buf.formatting_sync(nil, 2000)
    endif
  endfunction

  autocmd! BufWritePre * call DoFormatWrite()
  ]],
  true
)

M.toggle_formatting = function()
  vim.g.formatting_enabled = not vim.g.formatting_enabled
  print("Formatting is now", vim.g.formatting_enabled and "ON" or "OFF")
end

M.toggle_formatting_on_save = function()
  vim.g.formatting_on_save = not vim.g.formatting_on_save
  print("Formatting on Save is now", vim.g.formatting_on_save and "ON" or "OFF")
end

return M

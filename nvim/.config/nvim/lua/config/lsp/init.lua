local nvim_lsp = require("lspconfig")
local helpers = require("config/lsp/helpers")

-------------------------------------------------------------------------------
-- General LSP Setup
-------------------------------------------------------------------------------

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = {
  "dockerls",
  "gdscript",
  "gopls",
  "graphql",
  "jsonls",
  "pyright",
  "rust_analyzer",
  "vimls",
  "yamlls",
  'ansiblels'
}

for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup({
    init_options = { documentFormatting = false },
    on_attach = helpers.on_attach_no_formatting,
    flags = helpers.get_flags(),
  })
end

-------------------------------------------------------------------------------
-- Specialize LSP Setup
-------------------------------------------------------------------------------
require("config/lsp/gopls")
require("config/lsp/rust")
require("config/lsp/sqlls")
require("config/lsp/tsserver")
require("config/lsp/sumneko")

-------------------------------------------------------------------------------
-- Formatters / Diagnostics
-------------------------------------------------------------------------------
require("config/lsp/diaglist")
require("config/lsp/gutter")
require("config/lsp/null-ls")

-------------------------------------------------------------------------------
-- Misc. Tools
-------------------------------------------------------------------------------
require("config/lsp/symbols-outline")

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

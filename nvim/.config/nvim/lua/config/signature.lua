local map = require("utils").map
local whichkey = require("which-key")

map({ "n", "<space>ml", ":SignatureListBufferMarks<CR>" }, { silent = true })
whichkey.register({
  m = {
    l = { "List All Buffer Marks" },
  },
}, { prefix = "<space>" })

local whichkey = require("which-key")
local map = require("utils").map

vim.cmd([[
autocmd User Fugitive command! -buffer -nargs=? Ggraphlog <mods> Git
\ --paginate log
\ --oneline
\ --decorate
\ --graph
\ --all <args>
autocmd FileType fugitive nmap <buffer> za =
]])

local opts = { silent = true, noremap = true }

-- Normal Mode
map({ "n", "<space>gg", "<cmd>Git<CR>" }, opts)
map({ "n", "<space>gB", "<cmd>Git blame<CR>" }, opts)
map({ "n", "<space>gd", "<cmd>Gdiff<CR>" }, opts)
map({ "n", "<space>gl", "<cmd>Git log %<CR>" }, opts)
map({ "n", "<space>gL", "<cmd>Git log<CR>" }, opts)
map({ "n", "<space>gpl", "<cmd>Git pull<CR>" }, opts)
map({ "n", "<space>gpp", "<cmd>Git push --no-verify<CR>" }, opts)
map({ "n", "<space>gpP", "<cmd>Git push --no-verify<CR>" }, opts)
map({ "n", "<space>go", "<s-v>:GBrowse!<CR>" }, opts)

whichkey.register({
  g = {
    b = { "Blame" },
    d = { "Diff" },
    g = { "Open Git" },
    l = { "Log (Current File)" },
    L = { "Log (Whole Repo)" },
    o = { "Copy Link" },
    p = {
      l = { "Pull" },
      p = { "Push" },
      P = { "Push (No Verify)" },
    },
  },
}, { prefix = "<space>" })

-- Visual Mode
map({ "v", "<space>go", "<cmd>GBrowse!<CR>" }, opts)
whichkey.register({
  g = {
    o = { "Copy Link" },
  },
}, { mode = "v" })

local map = require("utils").map
local whichkey = require("which-key")

-- Disable default mappings
vim.g.scratch_no_mappings = false

-- Normal Mode
map({ "n", "<space>ns", ":Scratch<CR>", noremap = true })
whichkey.register({
  n = {
    s = { "Open Scratchpad" },
  },
}, { prefix = "<space>", mode = "n" })

local M = {}

-- Combine two tables into 1.  Modifies the first table.
M.concat = function(table1, table2)
  for k, v in pairs(table2) do
    table1[k] = v
  end
end

-- Compliments of: https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/
-- Keybind Helpers
M.map = function(key, opts)
  -- get the extra options
  opts = opts or { noremap = true }
  for i, v in pairs(key) do
    if type(i) == "string" then
      opts[i] = v
    end
  end

  -- basic support for buffer-scoped keybindings
  local buffer = opts.buffer
  opts.buffer = nil

  if buffer then
    vim.api.nvim_buf_set_keymap(0, key[1], key[2], key[3], opts)
  else
    vim.api.nvim_set_keymap(key[1], key[2], key[3], opts)
  end
end

return M

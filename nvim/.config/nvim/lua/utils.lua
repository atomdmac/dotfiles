local M = {}

-- Combine two tables into 1.  Modifies the first table.
M.concat = function(table1, table2)
	for k, v in pairs(table2) do
		table1[k] = v
	end
end

M.dump = function(o)
	if type(o) == "table" then
		local s = "{ "
		for k, v in pairs(o) do
			if type(k) ~= "number" then
				k = '"' .. k .. '"'
			end
			s = s .. "[" .. k .. "] = " .. dump(v) .. ","
		end
		return s .. "} "
	else
		return tostring(o)
	end
end

M.merge = function(...)
	local result = {}

	for _, tbl in ipairs({ ... }) do
		if type(tbl) == "table" then
			for key, value in pairs(tbl) do
				if type(value) == "table" and type(result[key]) == "table" then
					-- If both are tables, recursively merge them
					result[key] = M.merge(result[key], value)
				else
					-- Otherwise, assign the value to the result
					result[key] = value
				end
			end
		else
			error("All arguments must be tables")
		end
	end

	return result
end

-- Compliments of: https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/
-- Keybind Helpers
M.map = function(key, opts)
	-- get the extra options
	opts = opts or { noremap = true }
	for i, v in pairs(key) do
		if type(i) == "string" then
			opts[i] = v
		end
	end

	-- basic support for buffer-scoped keybindings
	local buffer = opts.buffer
	opts.buffer = nil

	if buffer then
		vim.api.nvim_buf_set_keymap(0, key[1], key[2], key[3], opts)
	else
		vim.api.nvim_set_keymap(key[1], key[2], key[3], opts)
	end
end

-- Compliments of: http://lua-users.org/wiki/FileInputOutput
-- see if the file exists
M.file_exists = function(file)
	local f = io.open(file, "rb")
	if f then
		f:close()
	end
	return f ~= nil
end

-- get all lines from a file, returns an empty
-- list/table if the file does not exist
M.lines_from = function(file)
	if not M.file_exists(file) then
		return {}
	end
	local lines = {}
	for line in io.lines(file) do
		lines[#lines + 1] = line
	end
	return lines
end

M.script_path = function()
	local str = debug.getinfo(2, "S").source:sub(2)
	return str:match("(.*/)")
end

-- Compliments of: https://stackoverflow.com/a/23535333
-- Lua implementation of PHP scandir function
M.scandir = function(directory)
	local i, t, popen = 0, {}, io.popen
	local pfile = popen('ls -a "' .. directory .. '"')
	for filename in pfile:lines() do
		i = i + 1
		t[i] = filename
	end
	pfile:close()
	return t
end

return M

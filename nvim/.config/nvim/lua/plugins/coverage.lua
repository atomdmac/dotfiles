local coverageLoaded = false

return {
	"andythigpen/nvim-coverage",
	version = "*",
	config = function()
		local coverage = require("coverage")
		coverage.setup({
			auto_reload = true,
		})

		local whichkey = require("which-key")
		whichkey.add({
			{ "<leader>c", group = "Code Coverage" },
			{
				"<leader>cc",
				function()
					-- In order for `toggle` to work, coverage has to be loaded first.
					-- Since the plugin doesn't handle this automatically, we'll do it here.
					if not coverageLoaded then
						coverage.load(true)
						coverageLoaded = true
					else
						coverage.toggle()
					end
				end,
				desc = "Toggle Code Coverage",
			},
			{ "<leader>cs", ":CoverageSummary<CR>", desc = "Coverage Summary" },
		})
	end,
}

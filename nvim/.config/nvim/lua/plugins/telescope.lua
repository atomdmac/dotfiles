local function telescope_project_files()
	local opts = {
		hidden = true,
		no_ignore = true,
	} -- define here if you want to define something
	local ok = pcall(require("telescope.builtin").git_files, opts)
	if not ok then
		require("telescope.builtin").find_files(opts)
	end
end

local function define_key_map()
	local telescope = require("telescope.builtin")
	local whichkey = require("which-key")

	whichkey.add({
		{
			"<space><BS>",
			telescope.buffers,
			desc = "Find Buffer",
		},
		{
			"<space><space>",
			telescope_project_files,
			desc = "Find File",
		},
		{ "<space>f", group = "Find / Select" },
		{ "<space>fa", telescope.find_files, desc = "File" },
		{ "<space>fb", telescope.buffers, desc = "Buffer" },
		{
			"<space>fj",
			telescope.jumplist,
			desc = "Jumplist",
		},
		{ "<space>fm", telescope.marks, desc = "Marks" },
		{ "<space>gc", telescope.git_branches, desc = "Branch" },
		{ "<space>gs", telescope.git_stash, desc = "Stash" },
		{ "<space>s", group = "Search for Text" },
		{
			"<space>sb",
			telescope.current_buffer_fuzzy_find,
			desc = "...in Buffer",
		},
		{
			"<space>ss",
			telescope.live_grep,
			desc = "...in Project",
		},
	})
end

local themes = {
	vertical_full_screen = {
		layout_strategy = "vertical",
		layout_config = {
			preview_height = 0.80,
			height = 0.9999999999999,
			width = 0.9999999999999,
		},
	},
}

local common = {
	mappings = {
		i = {
			["<esc>"] = "close",
		},
	},
}

return {
	"nvim-telescope/telescope.nvim",
	-- Define keymaps on init instead of keys  since we need to be able to
	-- reference the `telescope` module from within the define_key_map function.
	init = define_key_map,
	opts = {
		defaults = {
			prompt_prefix = "🔍 ",
			vimgrep_arguments = {
				"rg",
				"--sortr=modified",
				"--color=never",
				"--no-heading",
				"--with-filename",
				"--line-number",
				"--column",
				"--smart-case",
			},
			additional_args = {
				"--hidden",
			},
			mappings = {
				["i"] = {
					["<esc>"] = "close",
				},
			},
		},
		pickers = {
			git_files = {
				mappings = common.mappings,
				layout_strategy = "flex",
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
			},
			find_files = {
				hidden = true,
				mappings = common.mappings,
				layout_strategy = "flex",
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
			},
			buffers = {
				layout_strategy = "vertical",
				sort_mru = true,
				mappings = vim.tbl_extend("keep", {
					i = {
						["<c-d>"] = "delete_buffer",
					},
				}, common.mappings),
			},
			quickfix = {
				mappings = common.mappings,
				show_line = false,
				trim_text = true,
				layout_strategy = "flex",
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
			},
			git_stash = {
				mappings = common.mappings,
				layout_strategy = "flex",
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
			},
			git_branches = vim.tbl_extend(
				"keep",
				{},
				themes.vertical_full_screen,
				common.mappings
			),
			live_grep = {
				mappings = common.mappings,
				layout_strategy = "flex",
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
				additional_args = function()
					return { "--hidden", "--iglob", "!.git" }
				end,
			},
			current_buffer_fuzzy_find = {
				theme = "dropdown",
				mappings = common.mappings,
				layout_config = { height = 0.9999999999999, width = 0.9999999999999 },
			},
			marks = { theme = "dropdown", mappings = common.mappings },
			jumplist = { theme = "dropdown", mappings = common.mappings },
		},
	},
}

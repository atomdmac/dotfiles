-- Find tags in the notes directory using ripgrep.
-- Canabalized from the rg-cmp plugin.

local source = {}

source.new = function()
  local timer = vim.loop.new_timer()
  vim.api.nvim_create_autocmd("VimLeavePre", {
    callback = function()
      if timer and not timer:is_closing() then
        timer:stop()
        timer:close()
      end
    end,
  })
  return setmetatable({
    running_job_id = 0,
    timer = timer,
    json_decode = vim.fn.has("nvim-0.6") == 1 and vim.json.decode or vim.fn.json_decode,
  }, { __index = source })
end

source.get_trigger_characters = function()
  return { "@" }
end

source.get_keyword_pattern = function()
  return "@\\w*"
end

source.complete = function(self, request, callback)
  local candidates = {}
  -- This will be called multiple times, so we need to need to define the list
  -- outside of the callback so we can build it across multiple calls.
  local function on_event(_, data, event)
    if event == "stdout" then
      -- Build list of candiates using the line as the key.
      -- This will deduplicate the list.
      for _, line in ipairs(data) do
        candidates[line] = {
          label = line,
          insertText = line,
          kind = "Tag",
        }
      end

      -- Convert the map to a list.
      local items = {}
      for _, candidate in pairs(candidates) do
        table.insert(items, candidate)
      end

      callback({
        items = items,
        incomplete = true,
      })
    end
  end

  self.timer:stop()
  self.timer:start(
    request.option.debounce or 100,
    0,
    vim.schedule_wrap(function()
      vim.fn.jobstop(self.running_job_id)
      self.running_job_id = vim.fn.jobstart(
        "rg --no-heading --color=never --no-filename --hidden --no-line-number -o '@\\w+' ~/Notes",
        {
          on_stderr = on_event,
          on_stdout = on_event,
          on_exit = on_event,
          cwd = request.option.cwd or vim.fn.getcwd(),
        }
      )
    end)
  )
end

return source

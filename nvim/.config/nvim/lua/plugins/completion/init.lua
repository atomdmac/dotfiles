-- Auto-complete Setup
-- local cmp = require("cmp")
-- local lspkind = require("lspkind")

vim.g.completeopt = { "menu", "menuone", "noselect" }

return {
	"hrsh7th/nvim-cmp",
	dependencies = {
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-emoji",
		"hrsh7th/cmp-cmdline",
		"hrsh7th/cmp-nvim-lua",
		"lukas-reineke/cmp-rg",
		"andersevenrud/cmp-tmux",
		"f3Fora/cmp-spell",
		"hrsh7th/vim-vsnip",
		"hrsh7th/vim-vsnip-integ",
		{
			"onsails/lspkind-nvim",
			config = function()
				local lspkind = require("lspkind")
				lspkind.init({
					-- DEPRECATED (use mode instead): enables text annotations
					--
					-- default: true
					-- with_text = true,

					-- defines how annotations are shown
					-- default: symbol
					-- options: 'text', 'text_symbol', 'symbol_text', 'symbol'
					mode = "symbol_text",

					-- default symbol map
					-- can be either 'default' (requires nerd-fonts font) or
					-- 'codicons' for codicon preset (requires vscode-codicons font)
					--
					-- default: 'default'
					preset = "codicons",

					-- override preset symbols
					--
					-- default: {}
					symbol_map = {
						Text = "󰉿",
						Method = "󰆧",
						Function = "󰊕",
						Constructor = "",
						Field = "󰜢",
						Variable = "󰀫",
						Class = "󰠱",
						Interface = "",
						Module = "",
						Property = "󰜢",
						Unit = "󰑭",
						Value = "󰎠",
						Enum = "",
						Keyword = "󰌋",
						Snippet = "",
						Color = "󰏘",
						File = "󰈙",
						Reference = "󰈇",
						Folder = "󰉋",
						EnumMember = "",
						Constant = "󰏿",
						Struct = "󰙅",
						Event = "",
						Operator = "󰆕",
						TypeParameter = "",
					},
				})
			end,
		},
	},
	config = function(lazyPlugin, opts)
		local cmp = require("cmp")
		local lspkind = require("lspkind")

		local mapping = {
			["<C-d>"] = cmp.mapping.scroll_docs(-4),
			["<C-f>"] = cmp.mapping.scroll_docs(4),
			["<A-Space>"] = cmp.mapping.complete(),
			["<C-e>"] = cmp.mapping.close(),
			["<CR>"] = cmp.mapping.confirm(),
			["<DOWN>"] = cmp.mapping.select_next_item(),
			["<UP>"] = cmp.mapping.select_prev_item(),
		}

		local formatting = {
			format = lspkind.cmp_format({
				mode = "symbol",
				with_text = false, -- do not show text alongside icons
				maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)

				-- The function below will be called before any actual modifications from lspkind
				-- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
				-- before = function (entry, vim_item)
				-- ...
				-- return vim_item
				-- end
			}),
		}

		local finalConfig = vim.tbl_deep_extend("keep", {
			mapping = mapping,
			formatting = formatting,
		}, opts)

		-- Register the custom source
		cmp.register_source("rg_tags", require("plugins/completion/tag-search").new())

		cmp.setup(finalConfig)
	end,
	opts = {
		snippet = {
			expand = function(args)
				-- For `vsnip` user.
				vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` user.

				-- For `luasnip` user.
				-- require('luasnip').lsp_expand(args.body)

				-- For `ultisnips` user.
				-- vim.fn["vsnip#anonymous"](args.body)
			end,
		},
		sources = {
			{ name = "nvim_lsp" },
			{ name = "nvim_lua" },
			-- For vsnip user.
			{ name = "vsnip" }, 
			-- For luasnip user.
			-- { name = 'luasnip' },
			-- For ultisnips user.
			-- { name = 'ultisnips' },
			{ name = "buffer" },
			{ name = "emoji" },
			{ name = "path" },
			{ name = "spell" },
			{ name = "nvim_lua" },
			{ name = "mkdnflow" },
			{ name = "rg_tags" }
		},
	},
}

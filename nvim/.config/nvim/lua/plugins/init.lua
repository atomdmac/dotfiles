vim.g.loaded_matchit = 1

local plugins = {
	{ "nvim-lua/plenary.nvim", lazy = true },
	require("plugins/lsp"),
	require("plugins/trouble"),
  require("plugins/which-key"),
	require("plugins/treesitter"),
	require("plugins/tmux"),
	require("plugins/catppuccin"),
	require("plugins/lualine"),
	require("plugins/vcs/fugitive"),
	require("plugins/vcs/gitsigns"),
	require("plugins/vcs/diffview"),
	require("plugins/comment"),
	require("plugins/lsp/null-ls"),
	require("plugins/completion"),
	require("plugins/flash"),
	require("plugins/vifm"),
	require("plugins/telescope"),
	require("plugins/sessions"),
	require("plugins/copilot"),
	require("plugins/notes"),
	require("plugins/autopairs"),
	require("plugins/autotag"),
	require("plugins/mkdnflow"),
	require("plugins/quickfix"),
	require("plugins/blankline"),
	require("plugins/goto-preview"),
	require("plugins/nvim-web-devicons"),
	require("plugins/pastify"),
	require("plugins/codecompanion"),
	require("plugins/grug-far"),
	require("plugins/render-markdown"),
  require("plugins/coverage"),
	{ "kshenoy/vim-signature" },
	{ "wakatime/vim-wakatime" },
	{ "tpope/vim-surround" },
	{ "tpope/vim-abolish" },
	{ "tpope/vim-sleuth" },
}

-- Random utilities that I wrote
require("plugins/mkdn-tools")
require("plugins/file-tools")
require("plugins/todo")

return plugins

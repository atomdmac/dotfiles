local utils = require("utils")

local function init_vimscript_config()
	local vimConfigPath = utils.script_path() .. "/fugitive.vim"
	local vimConfig = utils.lines_from(vimConfigPath)
	vim.cmd(table.concat(vimConfig, "\n"))
end

return {
	"tpope/vim-fugitive",
	dependencies = {
		"tpope/vim-rhubarb",
		"shumphrey/fugitive-gitlab.vim",
	},
	init = function()
		init_vimscript_config()
		local whichkey = require("which-key")
		whichkey.add({
			{ "<space>g", group = "Git" },
			{ "<space>gN", ":Git checkout -b ", desc = "Create Branch" },
			{ "<space>gb", "<cmd>Git blame<CR>", desc = "Blame" },
			{ "<space>gd", "<cmd>Gdiff<CR>", desc = "Diff" },
			{ "<space>gg", "<cmd>Git<CR>", desc = "Open Git Window" },
			{ "<space>gl", "<cmd>Git log %<CR>", desc = "Log (Current File)" },
			{ "<space>go", "<s-v>:GBrowse!<CR>", desc = "Get Forge Link" },
			{ "<space>gp", group = "Pull/Push" },
			{ "<space>gpl", "<cmd>Git pull<CR>", desc = "Pull" },
			{ "<space>gpp", "<cmd>Git push --no-verify<CR>", desc = "Push" },
		})
	end,
}

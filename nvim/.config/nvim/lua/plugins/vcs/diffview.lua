return {
	"sindrets/diffview.nvim",
	init = function()
		local whichkey = require("which-key")
		whichkey.add({
			{ "<space>gL", "<cmd>DiffviewFileHistory<CR>", desc = "Explore History" },
		})
	end,
	opts = {
		keymaps = {
			view = {
				{
					"n",
					"<space>wd",
					"<cmd>DiffviewClose<CR>",
					{ desc = "Close History", noremap = true, silent = true },
				},
			},
			file_panel = {
				{
					"n",
					"<space>wd",
					"<cmd>DiffviewClose<CR>",
					{ desc = "Close History", noremap = true, silent = true },
				},
			},
			file_history_panel = {
				{
					"n",
					"<space>wd",
					"<cmd>DiffviewClose<CR>",
					{ desc = "Close History", noremap = true, silent = true },
				},
			},
		},
	},
}

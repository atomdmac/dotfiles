" Show formatted git log in a new buffer
autocmd User Fugitive command! -buffer -nargs=? Ggraphlog <mods> Git
\ --paginate log
\ --oneline
\ --decorate
\ --graph
\ --all <args>

" Use normal folding keymap for fugitive buffers
autocmd FileType fugitive nmap <buffer> za =

function s:OnExit(id, code, name)
  " Close all buffers opened as part of this process
  let buffers = filter(range(1, bufnr('$')), 'bufexists(v:val)')
  " If hook was successful, close the relavant buffers and run the original
  " Git command.
  if(a:code == 0)
    for b in buffers
      let info = getbufinfo(b)[0]
      let isHookOutput = has_key(l:info.variables, 'git_hook_output')
      if (isHookOutput)
        execute('bd' .. b)
      endif
    endfor
    execute("G commit --no-verify")
  endif
endfunction

function CommitWithStickyBuffer()
  " Mark this new log buffer as part of the hook output process
  let l:hookPath = ".git/hooks/pre-commit"
  if (executable(l:hookPath))
    new
    let b:git_hook_output = 1
    let g:term_id = termopen(l:hookPath,
      \ { 'on_exit': function("s:OnExit")
    \ })
    nmap <expr> q ":execute jobstop(" .. g:term_id .. ")<CR>"
    nmap <expr> <c-c> ":execute jobstop(" .. g:term_id .. ")<CR>"
  else
    execute("G commit --no-verify")
  endif
endfunction

" When running commit hooks, keep the buffer open so we can see what's happening.
" This is especially necessary for long-running hooks!
autocmd FileType fugitive nmap <buffer> cc :call CommitWithStickyBuffer()<CR>

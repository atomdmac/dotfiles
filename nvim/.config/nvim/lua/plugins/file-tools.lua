-- Rename the current file to a new name, prompting the user for input.
local function rename_current_file()
  -- Get the current file name
  local old_name = vim.fn.expand('%:p') -- Use the full file path

  -- If there is no file name, inform the user and abort
  if old_name == '' then
    print("No file to rename")
    return
  end

  -- Prompt the user for a new file name, pre-filling the old file name
  local new_name = vim.fn.input('Rename to: ', old_name, 'file')

  -- If the new name is empty or equal to the old name, the user cancelled or didn't change
  if new_name == '' or new_name == old_name then
    print("Rename cancelled")
    return
  end

  -- Save the current cursor position and window view
  local view = vim.fn.winsaveview()

  -- Begin undoable sequence
  vim.cmd('keepjumps normal! m`') -- Set a mark for jumping back
  vim.cmd('silent! write') -- Ensure buffer is written before renaming

  -- Perform the file rename operation
  -- Try to rename the file, and handle errors if any
  local success, err = pcall(vim.fn.rename, old_name, new_name)
  if not success then
    print("Rename failed: " .. err)
    return
  end

  -- Edit the new file name in the current buffer and delete the old buffer entry
  vim.cmd('edit ' .. new_name)
  vim.cmd('silent! write') -- Ensure the new buffer is written

  -- Restore the cursor position and window view
  vim.fn.winrestview(view)

  -- End undoable sequence
  vim.cmd('undojoin | silent! write') -- Join into a single undo block

  -- Inform the user of a successful rename
  print("File renamed to: " .. new_name)
end

-- Bind the function to a key combination if desired, e.g., <Leader>nr
vim.api.nvim_set_keymap('n', '<Leader>nr', '', { callback = rename_current_file, noremap = true, silent = true })

-- Rename the current file based on the contents of the current line.
local function rename_file_with_current_line()
    -- Get current line text
    local current_line = vim.api.nvim_get_current_line()

    -- Strip illegal file name characters, including whitespace at the beginning and end
    current_line = vim.fn.trim(current_line)

    -- Convert illegal characters to underscores and remove adjacent & trailing underscores
    current_line = string.gsub(current_line, "%s", "_")
    current_line = string.gsub(current_line, '["%<%>%{%}%(%)[%]%%,%^%&%*$=#]', "_")
    current_line = string.gsub(current_line, "'", "")
    current_line = string.gsub(current_line, "__+", "_")
    current_line = string.gsub(current_line, "^_", "")
    current_line = string.gsub(current_line, "_$", "")

    -- Get the current file name
    local current_file = vim.fn.expand('%:p')

    -- Construct the new file name
    local new_name = current_line .. '_' .. vim.fn.fnamemodify(current_file, ':t')

    -- Get the current directory
    local current_dir = vim.fn.fnamemodify(current_file, ':h')

    -- Prompt the user to review and change the new file name
    new_name = vim.fn.input('New file name: ', new_name)

    -- If the user presses Escape, input() returns an empty string, so cancel the operation
    if new_name == '' then
        print("Operation cancelled.")
        return
    end

    -- Ensure the user-reviewed file name does not have leading or trailing whitespace and remove illegal characters again
    new_name = vim.fn.trim(new_name)
    new_name = string.gsub(new_name, '[\\/:%*?"<>|%(%)[%]%,;%%$%!%^%&%*%#]', "")

    -- Construct the full new file path
    local new_file_path = current_dir .. '/' .. new_name

    -- Rename the file and handle possible errors
    local ok, err = pcall(function()
        os.rename(current_file, new_file_path)
        -- Update the buffer to the new file name
        vim.cmd('edit ' .. vim.fn.fnameescape(new_file_path))
        print("File renamed to " .. new_file_path)
    end)

    if not ok then
        print("Failed to rename file: " .. err)
    end
end

-- Map the function to a convenient key sequence for ease of use, e.g., <Leader>rn
vim.api.nvim_set_keymap('n', '<Leader>rn', '', { callback = rename_file_with_current_line, noremap = true, silent = true })

-- Define a function to search for TODO items
local function find_todos()
  -- Clear the quickfix list
  vim.fn.setqflist({})

  -- Check if ripgrep is available
  if vim.fn.executable('rg') == 1 then
    -- Use ripgrep to search for TODO items
    -- Adjust the pattern if you want to customize the TODO search pattern
    local rg_output = vim.fn.systemlist('rg --vimgrep TODO')
    -- Populate the quickfix list with ripgrep output
    local qf_list = {}
    for _, line in ipairs(rg_output) do
      local parts = vim.split(line, ':')
      table.insert(qf_list, {
        filename = parts[1],
        lnum = tonumber(parts[2]),
        col = tonumber(parts[3]),
        text = table.concat(vim.list_slice(parts, 4), ':') -- Join the remaining parts to get the whole line
      })
    end
    vim.fn.setqflist(qf_list)
  else
    -- Fall back to using Neovim's built-in search if ripgrep is not available
    vim.cmd('vimgrep /TODO/ **/*')
  end

  -- Open the quickfix list
  vim.cmd('copen')
end

-- Create a user command to run the find_todos function
vim.api.nvim_create_user_command('FindTODOs', find_todos, {})

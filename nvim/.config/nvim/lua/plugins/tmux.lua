return {
	"alexghergh/nvim-tmux-navigation",
	lazy = false,
	keys = {
		{ "<M-h>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateLeft()<CR>" },
		{ "<M-j>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateDown()<CR>" },
		{ "<M-k>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateUp()<CR>" },
		{ "<M-l>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateRight()<CR>" },
		{ "<M-\\>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateLastActive()<CR>" },
		{ "<M-Space>", "<cmd>lua require'nvim-tmux-navigation'.NvimTmuxNavigateNext()<CR>" },
	},
}

return {
	"natecraddock/sessions.nvim",
	lazy = false,
	init = function()
		-- Only load/save sessions if no files were passed in during startup
		-- Arg1 will always be the nvim binary
		-- Arg2 will always be "--embed"
		local argCount = vim.v.argv and #vim.v.argv or 0
		if argCount > 2 then
			return
		end

		local sessions = require("sessions")
		sessions.load()
		sessions.start_autosave()
	end,
	opts = {
		events = { "BufEnter", "VimLeavePre" },
		session_filepath = vim.fn.stdpath("data") .. "/sessions/",
		absolute = true,
	},
}

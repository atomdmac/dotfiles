local define_keymap = function()
	local whichkey = require("which-key")
	whichkey.add({
		{ "<space>ee", ":Vifm<CR>", desc = "...Using ViFM" },
	})
end

-- Disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Set Vifm as the default file explorer
vim.g.vifm_replace_netrw = 1

return {
	"vifm/vifm.vim",
	lazy = false,
	init = function()
		define_keymap()
	end,
}

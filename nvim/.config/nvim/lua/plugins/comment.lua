return {
	"numToStr/Comment.nvim",
	lazy = true,
	opts = {
		mappings = {
			basic = true,
			extra = true,
		},
	},
	init = function()
		local comment_api = require("Comment.api")
		local whichkey = require("which-key")

		-- Toggle in NORMAL mode
		whichkey.add({
			{ "<space>c", group = "Comments" },
			{ "<space>cc", comment_api.toggle.linewise.current, desc = "Toggle Block-Style Comment" },
			{ "<space>cb", comment_api.toggle.blockwise.current, desc = "Toggle Line-Style Comment" },
		})

		-- Toggle in VISUAL mode
		-- The dev of this plugin has fucked up the API so we have a mix of lua
		-- and vim-style bindings.
		whichkey.add({
			{
				mode = { "x" },
				{ "<space>c", group = "Comments" },
				{ "<space>cb", "<Plug>(comment_toggle_blockwise_visual)", desc = "Toggle Block-Style Comment" },
				{ "<space>cc", "<Plug>(comment_toggle_linewise_visual)", desc = "Toggle Line-Style Comment" },
			},
		})

		-- Toggle in Op-pending mode
		whichkey.add({
			{ "gb", desc = "<Plug>(comment_toggle_blockwise)" },
			{ "gc", desc = "<Plug>(comment_toggle_linewise)" },
		})
	end,
}

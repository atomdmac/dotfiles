return {
	"jakewvincent/mkdnflow.nvim",
	init = function()
		local whichkey = require("which-key")
		whichkey.add({
			{ "<space>m", group = "Markdown" },
			{ "<space>mm", "<cmd>MkdnToggleToDo<CR>", desc = "Create / Toggle TODO" },
			{ "<space>mt", group = "Table" },
			{ "<space>mtc", "<cmd>MkdnTableNewColAfter<CR>", desc = "New Column" },
			{ "<space>mtf", "<cmd>MkdnTableFormat<CR>", desc = "Format Table" },
			{ "<space>mtn", "<cmd>MkdnTable 2 2<CR>", desc = "Create Table" },
			{ "<space>mtr", "<cmd>MkdnTableNewRowBelow<CR>", desc = "New Row" },
		})
	end,
	opts = {
		modules = {
			bib = true,
			buffers = true,
			conceal = true,
			cursor = true,
			folds = true,
			links = true,
			lists = true,
			maps = true,
			paths = true,
			tables = true,
			cmp = true,
			yaml = false,
		},
		filetypes = { md = true, rmd = true, markdown = true },
		create_dirs = true,
		perspective = {
			-- How to interpret links within the current file.
			priority = "current",
			fallback = "first",
			root_tell = false,
			nvim_wd_heel = false,
			update = false,
		},
		wrap = false,
		bib = {
			default_path = nil,
			find_in_root = true,
		},
		silent = false,
		links = {
			style = "markdown",
			name_is_source = false,
			conceal = true,
			context = 0,
			-- implicit_extension = nil,
			-- transform_implicit = false,
			-- transform_explicit = function(text)
			--      local current_file_path = vim.fn.expand('%')
			--      local current_file_dir = vim.fn.fnamemodify(current_file_path, ':h')
			-- 	text = text:gsub(" ", "-")
			-- 	text = text:lower()
			-- 	text = os.date("%Y-%m-%d_") .. text
			-- 	return text
			-- end,
		},
		new_file_template = {
			use_template = false,
			placeholders = {
				before = {
					title = "link_title",
					date = "os_date",
				},
				after = {},
			},
			template = "# {{ title }}",
		},
		to_do = {
			symbols = { " ", "-", "x" },
			update_parents = true,
			not_started = " ",
			in_progress = "-",
			complete = "x",
		},
		tables = {
			trim_whitespace = true,
			format_on_move = true,
			auto_extend_rows = false,
			auto_extend_cols = false,
		},
		yaml = {
			bib = { override = false },
		},
		mappings = {
			MkdnEnter = { { "n", "v" }, "<CR>" },
			MkdnTab = false,
			MkdnSTab = false,
			MkdnNextLink = { "n", "<Tab>" },
			MkdnPrevLink = { "n", "<S-Tab>" },
			MkdnNextHeading = { "n", "]]" },
			MkdnPrevHeading = { "n", "[[" },
			MkdnGoBack = { "n", "<BS>" },
			MkdnGoForward = { "n", "<Del>" },
			MkdnCreateLink = false, -- see MkdnEnter
			MkdnCreateLinkFromClipboard = { { "n", "v" }, "<leader>p" }, -- see MkdnEnter
			MkdnFollowLink = false, -- see MkdnEnter
			MkdnDestroyLink = { "n", "<M-CR>" },
			MkdnTagSpan = { "v", "<M-CR>" },
			MkdnMoveSource = { "n", "<F2>" },
			MkdnYankAnchorLink = { "n", "yaa" },
			MkdnYankFileAnchorLink = { "n", "yfa" },
			MkdnIncreaseHeading = { "n", "+" },
			MkdnDecreaseHeading = { "n", "-" },
			MkdnToggleToDo = { { "n", "v" }, "<C-Space>" },
			MkdnNewListItem = false,
			MkdnNewListItemBelowInsert = { "n", "o" },
			MkdnNewListItemAboveInsert = { "n", "O" },
			MkdnExtendList = false,
			MkdnUpdateNumbering = { "n", "<leader>nn" },
			MkdnTableNextCell = false, -- { "i", "<Tab>" },
			MkdnTablePrevCell = false, -- { "i", "<S-Tab>" },
			MkdnTableNextRow = false,
			MkdnTablePrevRow = { "i", "<M-CR>" },
			MkdnTableNewRowBelow = { "n", "<leader>ir" },
			MkdnTableNewRowAbove = { "n", "<leader>iR" },
			MkdnTableNewColAfter = { "n", "<leader>ic" },
			MkdnTableNewColBefore = { "n", "<leader>iC" },
			MkdnFoldSection = { "n", "<leader>f" },
			MkdnUnfoldSection = { "n", "<leader>F" },
		},
	},
}

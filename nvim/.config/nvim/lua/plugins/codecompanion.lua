return {
	"olimorris/codecompanion.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-treesitter/nvim-treesitter",
		"nvim-telescope/telescope.nvim", -- Optional
		{
			"stevearc/dressing.nvim", -- Optional: Improves the default Neovim UI
			opts = {},
		},
	},
	opts = {
		adapters = {
			openai = function()
				return require("codecompanion.adapters").use("openai", {
					env = {
						api_key = "cmd:pass show openai",
					},
				})
			end,
		},
	},
	config = true,
	init = function(opts)
		local whichkey = require("which-key")
		whichkey.add({
			{ "<space>a", group = "Code Companion" },
			{ "<space>aa", "<cmd>CodeCompanionActions<cr>", desc = "Actions" },
			{ "<space>at", "<cmd>CodeCompanionToggle<cr>", desc = "Toggle" },
		})

		-- Expand 'cc' into 'CodeCompanion' in the command line
		vim.cmd([[cab cc CodeCompanion]])
	end,
}

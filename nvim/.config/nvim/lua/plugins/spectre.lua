local utils = require("utils")

local define_key_map = function()
	local whichkey = require("which-key")

	whichkey.add({
		{ "<space>sp", "<cmd>lua require('spectre').open()<CR>", desc = "Search Project" },
	})

	whichkey.add({
		-- NOTE: It's necessary to use the string version of this command as
		-- written due to a strange bug where the selected text that is passed
		-- in as the search term "lags" one step behind the actual selection.
		{ "<space>sp", ":lua require('spectre').open_visual()<CR>)", desc = "Search Project", mode = "v" },
	})
end

local init_vimscript_config = function()
	local vimScriptPath = utils.script_path() .. "/spectre.vim"
	local vimScriptConfig = utils.lines_from(vimScriptPath)
	vim.cmd(table.concat(vimScriptConfig, "\n"))
end

return {
	-- "nvim-pack/nvim-spectre",
	"mkubasz/nvim-spectre",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	opts = {
		open_cmd = "new",
	},
	init = function()
		define_key_map()
		init_vimscript_config()
	end,
}

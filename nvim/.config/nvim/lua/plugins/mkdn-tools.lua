local function rename_markdown_link()
    local current_win = vim.api.nvim_get_current_win()
    local current_buf = vim.api.nvim_get_current_buf()
    local initial_cursor_pos = vim.api.nvim_win_get_cursor(current_win)
    local current_file_dir = vim.fn.expand('%:p:h')
    local line = vim.api.nvim_get_current_line()
    local col = initial_cursor_pos[2] + 1 -- Lua indexing starts from 1
    local pattern = '%[(.-)%]%((.-)%)'
    
    local link_text, file_path = string.match(line, pattern)
    local link_start, link_text_end = string.find(line, "%[.-%]")
    local file_start, file_path_end = string.find(line, "%(.-%)")

    -- Adjustments
    file_start = file_start + 1  -- skip the '(' character
    file_path_end = file_path_end - 1  -- skip the ')' character

    if link_text == nil or file_path == nil then
        print("Cursor is not on a markdown link.")
        return
    end

    local full_path = current_file_dir .. '/' .. file_path

    if vim.fn.filereadable(full_path) == 0 then
        print("File does not exist at path: " .. full_path)
        return
    end

    local new_link_text, new_file_name

    -- Determine cursor position: over link text or file path
    if col >= (link_start + 1) and col <= (link_text_end - 1) then
        new_link_text = vim.fn.input('Enter new link text: ', link_text)
        if new_link_text == '' then
            print("Operation cancelled.")
            return
        end
        new_file_name = new_link_text:gsub("%s+", "_"):gsub("%W", "_") .. ".md"  -- Transform to a valid file name convention
    elseif col >= file_start and col <= file_path_end then
        new_file_name = vim.fn.input('Enter new file name: ', file_path)
        if new_file_name == '' then
            print("Operation cancelled.")
            return
        end
        new_link_text = link_text
    else
        print("Cursor is not correctly placed over the markdown link.")
        return
    end

    local new_full_path = current_file_dir .. '/' .. new_file_name
    os.rename(full_path, new_full_path)

    local new_line = string.gsub(line, pattern, '[' .. new_link_text .. '](' .. new_file_name .. ')')
    vim.api.nvim_set_current_line(new_line)

    for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
        if vim.fn.bufname(bufnr) == full_path then
            vim.api.nvim_buf_set_name(bufnr, new_full_path)
            vim.cmd('buffer ' .. bufnr)
            vim.cmd('edit!')
            break
        end
    end

    -- Restoring the user's original context
    vim.api.nvim_set_current_win(current_win)
    vim.api.nvim_set_current_buf(current_buf)
    vim.api.nvim_win_set_cursor(current_win, initial_cursor_pos)

    print("File renamed and link updated.")
end

vim.api.nvim_set_keymap('n', '<leader>rl', '', { callback = rename_markdown_link, noremap = true, silent = true })

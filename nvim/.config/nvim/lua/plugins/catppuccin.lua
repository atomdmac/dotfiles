return {
	"catppuccin/nvim",
	lazy = false,
	priority = 9999999,
	config = function()
		vim.cmd([[
      colorscheme catppuccin
    ]])
	end,
}

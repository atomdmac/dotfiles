vim.g.lcov_coverage_files = {
	"lcov.info",
	"coverage.lcov",
	"coverage/lcov.info",
	"coverage/jest/lcov.info",
}

return {
	"atomdmac/vim-lcov",
	init = function()
		local whichkey = require("which-key")
		whichkey.add({
			{ "<leader>c", group = "Code Coverage" },
			{ "<leader>cc", ":LcovShow<cr>", desc = "Show Code Coverage" },
			{ "<leader>ch", ":LcovHide<cr>", desc = "Hide Code Coverage" },
		})
	end,
}

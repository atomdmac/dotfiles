return {
	"dracula/vim",
	lazy = false,
	priority = 9999999,
	config = function()
		vim.cmd([[
      colorscheme dracula
    ]])
	end,
}

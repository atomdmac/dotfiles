local get_options = function()
	local null_ls = require("null-ls")
	local common = require("plugins/lsp/common")
	return {
		on_attach = common.on_attach,
		sources = {
			null_ls.builtins.formatting.prettier.with({
				filetypes = {
					"javascript",
					"javascriptreact",
					"typescript",
					"typescriptreact",
					"html",
					"json",
					"tsx",
					"jsx",
					"css",
					"svelte",
				},
				condition = function(utils)
					vim.g.formatting_enabled = utils.root_has_file({
						".prettierrc.json",
						".prettierrc.js",
						".prettierrc",
					})
					return true
				end,
				runtime_condition = function()
					if vim.g.formatting_enabled ~= nil then
						return vim.g.formatting_enabled
					else
						return false
					end
				end,
				prefer_local = "node_modules/.bin",
			}),
			null_ls.builtins.formatting.black,
			null_ls.builtins.formatting.stylua,
			null_ls.builtins.formatting.sqlfluff.with({
				extra_args = { "--dialect", "postgres" },
			}),
			null_ls.builtins.diagnostics.sqlfluff.with({
				extra_args = { "--dialect", "postgres" },
			}),
		},
	}
end

return {
	"nvimtools/none-ls.nvim",
	opts = get_options,
}

local common = require("plugins/lsp/common")

return {
	init_options = { documentFormatting = false },
	flags = common.get_flags(),
	on_attach = function(client, bufnr)
		common.on_attach_no_formatting(client, bufnr)
	end,
	filetypes = { "typescript", "typescriptreact", "typescript.tsx" },
}

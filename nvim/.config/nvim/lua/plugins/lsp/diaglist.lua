return {
	"onsails/diaglist.nvim",
	init = function()
		require("diaglist").init({
			debug = false,
			debounce_ms = 150,
		})
	end,
}

-- Adjust log levels
-- Consule vim.lsp.log_levels for all possible values
vim.lsp.set_log_level(vim.lsp.log_levels.WARN)

-- Logging Format
if vim.fn.has("nvim-0.5.1") == 1 then
	require("vim.lsp.log").set_format_func(vim.inspect)
end

-- LSPs to initialize
local servers = {
	dockerls = {},
	eslint = {},
	gdscript = {},
	gopls = {},
	jsonls = {},
	lua_ls = {
		format = {
			enable = false,
		},
	},
	pyright = {},
	bashls = {},
	rust_analyzer = {},
	-- tailwindcss = {},
	flow = {},
	vimls = {},
	yamlls = {},
	-- vtsls = {},
	ts_ls = require("plugins/lsp/tsserver"),
	-- ansiblels = {},
}

return {
	"neovim/nvim-lspconfig",
	lazy = false,
	dependencies = {
		-- For some reason, this dep causes LSPs not to load for the first file opened
		-- require("plugins/lsp/diaglist"),
	},
	init = function()
		local lspconfig = require("lspconfig")
		local common = require("plugins/lsp/common")

		for server, config in pairs(servers) do
			lspconfig[server].setup(vim.tbl_deep_extend("keep", config, {
				flags = common.get_flags(),
				on_attach = common.on_attach,
			}))
		end
	end,
}

local M = {}

M.on_attach = function(client, bufnr)
	local whichkey = require("which-key")

	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	-- Set autocommands conditional on server_capabilities
	if client.server_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
      set updatetime=500
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]],
			false
		)
	end

	-- Key Mappings.
	vim.keymap.set(
		"n",
		"<space>lh",
		"<cmd>lua vim.lsp.buf.hover()<CR>",
		{ silent = true, noremap = true, desc = "Hover" }
	)
	vim.keymap.set(
		"n",
		"<leader>ff",
		"<cmd>lua vim.lsp.buf.format()<CR>",
		{ silent = true, noremap = true }
	)
	vim.keymap.set(
		"n",
		"<leader>ft",
		"<cmd>lua require('config/lsp/formatting').toggle_formatting()<CR>",
		{ silent = true, noremap = true, desc = "Toggle Formatting" }
	)
	vim.keymap.set(
		"n",
		"<leader>fs",
		"<cmd>lua require('config/lsp/formatting').toggle_formatting_on_save()<CR>",
		{ silent = true, noremap = true, desc = "Toggle Formatting on Save" }
	)
	vim.keymap.set(
		"n",
		"[e",
		"<cmd>lua vim.diagnostic.goto_prev()<CR>",
		{ silent = true, noremap = true, desc = "Go to Previous Error" }
	)
	vim.keymap.set(
		"n",
		"]e",
		"<cmd>lua vim.diagnostic.goto_next()<CR>",
		{ silent = true, noremap = true, desc = "Go to Next Error" }
	)
	vim.keymap.set(
		"n",
		"<space>ld",
		"<cmd>lua vim.lsp.buf.definition()<CR>",
		{ silent = true, noremap = true, desc = "Go to Definition" }
	)
	vim.keymap.set(
		"n",
		"<space>lr",
		"<cmd>lua vim.lsp.buf.rename()<CR>",
		{ silent = true, noremap = true, desc = "Rename Symbol" }
	)
	vim.keymap.set(
		"n",
		"<space>lu",
		"<cmd>lua vim.lsp.buf.references()<CR>",
		{ silent = true, noremap = true, desc = "List References" }
	)
	vim.keymap.set(
		"n",
		"<space>la",
		"<cmd>lua vim.lsp.buf.code_action()<CR>",
		{ silent = true, noremap = true, desc = "Code Actions" }
	)
	vim.keymap.set(
		"n",
		"<space>ls",
		"<cmd>lua vim.lsp.buf.signature_help()<CR>",
		{ silent = true, noremap = true, desc = "Signature Help" }
	)
	vim.keymap.set(
		"n",
		"<space>gi",
		"<cmd>lua vim.lsp.buf.implementation()<CR>",
		{ silent = true, noremap = true, desc = "Go to Implementation" }
	)
	vim.keymap.set(
		"n",
		"<space>lD",
		"<cmd>lua require('diaglist').open_all_diagnostics()<CR>",
		{ silent = true, noremap = true, desc = "Open All Diagnostics" }
	)

	-- Document keymaps via which-key
	whichkey.add({
		{
			"<space>l",
			group = "LSP",
		},
	})
end

-- Disable formatting for client on attach
M.on_attach_no_formatting = function(client, bufnr)
	-- Handle formatting with null-ls
	-- Older version of the setting
	client.server_capabilities.document_formatting = false
	-- Newer version of the setting
	client.server_capabilities.documentFormattingProvider = false

	M.on_attach(client, bufnr)
end

M.get_flags = function()
	return { debounce_text_changes = 150 }
end

return M

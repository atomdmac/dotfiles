local buffer_mode_status = {
	function()
		local mode = vim.api.nvim_get_mode().mode
		return string.upper(mode)
	end,
	refresh = {
		statusline = 250,
	},
}

return {
	"nvim-lualine/lualine.nvim",
	opts = {
		options = {
			theme = "dracula",
			icons_enabled = true,
		},
		component_separators = { left = "", right = "" },
		section_separators = { left = "", right = "" },
		sections = {
			lualine_a = { buffer_mode_status },
			lualine_b = { "diagnostics" },
			lualine_c = { "filename" },
			lualine_x = { "searchcount" },
			lualine_y = { "progress" },
			lualine_z = { "location" },
		},
		extensions = {
			"symbols-outline",
			"toggleterm",
			"quickfix",
		},
	},
}

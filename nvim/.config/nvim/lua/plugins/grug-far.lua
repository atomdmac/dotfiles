return {
	"MagicDuck/grug-far.nvim",
	config = function()
		local grug = require("grug-far")
		grug.setup({
			windowCreationCommand = "split", -- 'vsplit' or 'split'
			openTargetWindow = {
				preferredLocation = "below"
			},
			startInInsertMode = false,
			-- options, see Configuration section below
			-- there are no required options atm
			-- engine = 'ripgrep' is default, but 'astgrep' can be specified
			prefills = {
				search = "",
				replace = "",
				file = "",
				flags = "-i",
			},
			keymaps = {
				close = '<space>wd'
			}
		})

		local whichkey = require("which-key")
		function open_in_visual_mode()
			grug.open({ prefills = { search = vim.fn.expand("<cword>") } })
		end

		whichkey.add({
			{ "<space>s", group = "Search" },
			{ "<space>sp", grug.open, desc = "Find/Replace", noremap = true, silent = true, mode = "n" },
			{ "<space>sp", open_in_visual_mode, desc = "Find/Replace", noremap = true, silent = true, mode = "v" },
		})
	end,
}

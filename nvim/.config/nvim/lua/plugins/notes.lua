vim.g.notes_directory = "~/Notes/"
vim.g.notes_auto_rename = 0
vim.g.scratch_no_mappings = 1

return {
	"https://codeberg.org/atomdmac/notes.vim.git",
	dependencies = {
		"mtth/scratch.vim",
	},
	init = function()
		local whichkey = require("which-key")
		local telescope = require("telescope.builtin")

		local function live_search_notes()
			local opts = {
				cwd = vim.g.notes_directory,
				prompt_title = "Notes",
			}
			telescope.live_grep(opts)
		end

		whichkey.add({
			{ "<space>n", group = "Notes" },
			{ "<space>nl", ":NotesList<CR>", desc = "List Notes" },
			{ "<space>nm", ":NotesManage<CR>", desc = "Manage Notes" },
			{ "<space>nn", ":NotesCreate<CR>", desc = "New Note" },
			{ "<space>ns", ":NotesSearch<CR>", desc = "Search Notes" },
			{ "<space>nt", ":Scratch<CR>", desc = "Temp Buffer" },
			{ "<space>n<space>", live_search_notes, desc = "Live Search" },
		})
	end,
}

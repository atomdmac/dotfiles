-- Alias Plug command to make things look nicer.
local Plug = vim.fn["plug#"]

-- Install plugins
vim.call("plug#begin", "~/.config/nvim/plugged")

-- -----------------------------------------------------------------------------
-- plugins-snippets
-- -----------------------------------------------------------------------------
Plug("numToStr/Comment.nvim")
Plug("JoosepAlviste/nvim-ts-context-commentstring")

-- -----------------------------------------------------------------------------
-- plugins-autocomplete
-- -----------------------------------------------------------------------------
-- LSP Stuff
Plug("neovim/nvim-lspconfig")
Plug("kyazdani42/nvim-web-devicons")
Plug("folke/lsp-colors.nvim")
Plug("folke/trouble.nvim")
Plug("simrat39/rust-tools.nvim")
Plug("onsails/lspkind-nvim")
Plug("onsails/diaglist.nvim")
Plug("jose-elias-alvarez/nvim-lsp-ts-utils")

-- Auto-Complete
Plug("hrsh7th/vim-vsnip")
Plug("hrsh7th/vim-vsnip-integ")
Plug("hrsh7th/cmp-nvim-lsp")
Plug("hrsh7th/cmp-buffer")
Plug("hrsh7th/cmp-path")
Plug("hrsh7th/cmp-emoji")
Plug("hrsh7th/cmp-cmdline")
Plug("hrsh7th/cmp-nvim-lua")
Plug("lukas-reineke/cmp-rg")
Plug("atomdmac/cmp-vimwiki-tags", { branch = "at-syntax-support" })
Plug("f3Fora/cmp-spell")
Plug("hrsh7th/nvim-cmp")
Plug("windwp/nvim-ts-autotag")

-- -----------------------------------------------------------------------------
-- plugins-syntax
-- -----------------------------------------------------------------------------
Plug("nvim-treesitter/nvim-treesitter", { ["do"] = ":TSUpdate" })
Plug("posva/vim-vue")
Plug("plasticboy/vim-markdown")
Plug("masukomi/vim-markdown-folding")
Plug("maxmellon/vim-jsx-pretty")
Plug("pangloss/vim-javascript")
Plug("HerringtonDarkholme/yats.vim")
Plug("jparise/vim-graphql")
Plug("cespare/vim-toml")
Plug("habamax/vim-godot")

-- -----------------------------------------------------------------------------
-- plugins-tool
-- -----------------------------------------------------------------------------
-- Utility functions to be used by other plugins.
Plug("nvim-lua/plenary.nvim")
-- Terminal
Plug("akinsho/toggleterm.nvim")
-- File explorer
Plug("francoiscabrol/ranger.vim")
Plug("vifm/vifm.vim")
Plug("rbgrouleff/bclose.vim")
-- Git tools for Vim
Plug("tpope/vim-fugitive")
Plug("tpope/vim-rhubarb")
Plug("shumphrey/fugitive-gitlab.vim")
Plug("lewis6991/gitsigns.nvim")
-- Surround sections of text with things like brackets, quotes, etc.
Plug("tpope/vim-surround")
-- Enhancements to Vim's sessions
Plug("thaerkh/vim-workspace")
-- Indent guides
Plug("lukas-reineke/indent-blankline.nvim")
-- Note-taking
Plug("https://gitlab.com/atomdmac/notes.vim")
Plug("vimwiki/vimwiki")
-- Preview Markdown formatting in the browser
Plug("iamcco/markdown-preview.nvim", { ["do"] = "cd app && yarn install" })
-- Temporary buffer for note-taking
Plug("mtth/scratch.vim")
-- File / Buffer / Fuzzy Search
Plug("nvim-telescope/telescope.nvim")
Plug("nvim-telescope/telescope-fzf-native.nvim", { ["do"] = "make" })
Plug("nvim-telescope/telescope-file-browser.nvim")
Plug("nvim-telescope/telescope-symbols.nvim")
Plug("nvim-telescope/telescope-github.nvim")
Plug("tknightz/telescope-termfinder.nvim")
-- Unix Utilties / Helpers
Plug("tpope/vim-eunuch")
-- Better repeat support for compound commands
Plug("tpope/vim-repeat")
-- Making moving around buffers easier.
-- Plug("rlane/pounce.nvim")
Plug("phaazon/hop.nvim")
-- Search/Replace & Casing Tool
Plug("tpope/vim-abolish")
Plug("wakatime/vim-wakatime")
-- Find / Replace
Plug("nvim-pack/nvim-spectre")
-- Pair braces, quotes, etc.
Plug("windwp/nvim-autopairs")
-- Jump between matching characters
Plug("andymass/vim-matchup")
-- Themes/Colors/Etc.
Plug("akinsho/bufferline.nvim")
Plug("nvim-lualine/lualine.nvim")
Plug("srcery-colors/srcery-vim")
Plug("morhetz/gruvbox")
Plug("chrisbra/Colorizer")
Plug("kshenoy/vim-signature")
Plug("ryanoasis/vim-devicons")
Plug("sainnhe/edge")
Plug("jacoborus/tender.vim")
Plug("Rigellute/shades-of-purple.vim")
Plug("sainnhe/sonokai")
Plug("rafalbromirski/vim-aurora")
Plug("folke/tokyonight.nvim")
Plug("rebelot/kanagawa.nvim")
Plug("dracula/vim")
-- UI to display keybinds.
Plug("folke/which-key.nvim")
-- Display code context based on cursor position.
Plug("SmiteshP/nvim-gps")
-- Unit Testing
Plug("atomdmac/vim-lcov")
-- Code Linting/Formatters
Plug("jose-elias-alvarez/null-ls.nvim")
-- Nicer-looking notifications
Plug("rcarriga/nvim-notify")
-- Nicer-looking UI inputs
Plug("stevearc/dressing.nvim")
-- Tool for converting between one-line and multi-line arrow functions
Plug("AndrewRadev/splitjoin.vim")
-- Improvements for folds
Plug("anuvyklack/pretty-fold.nvim")
Plug("anuvyklack/nvim-keymap-amend")

-- Testing!
Plug("hkupty/iron.nvim")
Plug("simrat39/symbols-outline.nvim")
Plug("folke/twilight.nvim")
Plug("alexghergh/nvim-tmux-navigation")

vim.call("plug#end")


-- Use my .vimrc
vim.cmd("source ~/.vimrc")

-- Install Lazy plugin manager if it doesn't exist.
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = require("plugins")

-- Make sure to set `mapleader` before lazy so your mappings are correct
vim.g.mapleader = "\\"

-- Load plugin manager and plugins.
require("lazy").setup(plugins)

-- -----------------------------------------------------------------------------
-- options-vim-jsx
-- -----------------------------------------------------------------------------
vim.g.jsx_ext_required = false

-- -----------------------------------------------------------------------------
-- options-misc
-- -----------------------------------------------------------------------------
-- Use FileType Plug-ins
vim.cmd("filetype plugin on")

-- HACK: Uncomment this when running :PlugInstall or :PlugUpdate
-- Helps prevent an issue where running these commands hangs in WSL
-- vim.cmd([[
-- let g:loaded_clipboard_provider = 1
-- let $NVIM_NODE_LOG_FILE='nvim-node.log'
-- let $NVIM_NODE_LOG_LEVEL='warn'
-- ]])

-- Always show the signcolumn, otherwise it would shift the text each time
-- diagnostics appear/become resolved.
if vim.fn.has("patch-8.1.1564") == true then
  -- Recently vim can merge signcolumn and number column into one
  vim.g.signcolumn = "number"
else
  vim.g.signcolumn = "yes"
end

-- Set font (for GUIs)
vim.g.guifont = "Hasklug NF:w08"

-- Include .lua and init.lua files in runtime path
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require('nvim-treesitter.install').prefer_git = true

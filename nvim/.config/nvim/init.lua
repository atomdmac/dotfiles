-- Use my .vimrc
vim.cmd("source ~/.vimrc")

-- -----------------------------------------------------------------------------
-- Helper Functions
-- -----------------------------------------------------------------------------
local map = require("utils").map

-- -----------------------------------------------------------------------------
-- Import Plugins
-- -----------------------------------------------------------------------------
require("plugins")

-- Experimental Settings / Plugin Testing

-- We'll configure this first since other modules/plugins will rely on it to
-- create new keybinds.
require("which-key").setup()

-- -----------------------------------------------------------------------------
-- Load Configs
-- -----------------------------------------------------------------------------
-- require("config/pounce")
require("config/autopair")
require("config/autotag")
require("config/bufferline")
require("config/comment")
require("config/completion")
require("config/dressing")
require("config/fugitive")
require("config/gitsigns")
require("config/hop")
require("config/indent-blankline")
require("config/lcov")
require("config/lsp")
require("config/lualine")
require("config/macosx")
require("config/neovide")
require("config/notes")
require("config/notify")
require("config/pretty-fold")
require("config/ranger")
require("config/rust")
require("config/scratch")
require("config/signature")
require("config/spectre")
require("config/telescope")
require("config/theme")
require("config/tmux")
require("config/toggleterm")
require("config/treesitter")
require("config/vifm")
require("config/vim-markdown")
require("config/vimwiki")
require("config/workspace")

-- -----------------------------------------------------------------------------
-- 📔 options-vimwiki
-- -----------------------------------------------------------------------------
-- See /plugins/vimwiki.vim

-- -----------------------------------------------------------------------------
-- options-vim-jsx
-- -----------------------------------------------------------------------------
vim.g.jsx_ext_required = false

-- -----------------------------------------------------------------------------
-- options-misc
-- -----------------------------------------------------------------------------
-- Use FileType Plug-ins
vim.cmd("filetype plugin on")

-- HACK: Uncomment this when running :PlugInstall or :PlugUpdate
-- Helps prevent an issue where running these commands hangs in WSL
-- vim.cmd([[
-- let g:loaded_clipboard_provider = 1
-- let $NVIM_NODE_LOG_FILE='nvim-node.log'
-- let $NVIM_NODE_LOG_LEVEL='warn'
-- ]])

-- Show config directory
map({ "n", "<leader>co", ":e ~/.dotfiles/dotfiles/home<CR>" })

-- Always show the signcolumn, otherwise it would shift the text each time
-- diagnostics appear/become resolved.
if vim.fn.has("patch-8.1.1564") == true then
  -- Recently vim can merge signcolumn and number column into one
  vim.g.signcolumn = "number"
else
  vim.g.signcolumn = "yes"
end

-- Set font (for GUIs)
vim.g.guifont = "Hasklug NF:w08"

-- Include .lua and init.lua files in runtime path
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

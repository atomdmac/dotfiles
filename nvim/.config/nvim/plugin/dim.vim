hi DimVim guifg=#555555
hi Normal guibg=none cterm=none

function! s:SyntaxOn()
  set syntax=on
endfunction

function! s:SyntaxOff()
  syntax region DimVim start='' end='$$$end$$$'
endfunction

augroup DimVim
  autocmd!
  autocmd FocusGained * call s:SyntaxOn()
  autocmd FocusLost * call s:SyntaxOff()
augroup END

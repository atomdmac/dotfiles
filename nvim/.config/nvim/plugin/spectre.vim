let s:indices = [3, 5, 7]

function GetNextFieldIndex()
  let pos = getpos('.')
  let nextPos = s:indices[0]
  for i in s:indices
    if (i > l:pos[1])
      let nextPos = i
      break
    endif
  endfor
  return nextPos
endfunction

function GetPrevFieldIndex()
  let pos = getpos('.')
  let prevPos = l:pos[1]
  let lastFieldLine = s:indices[len(s:indices) - 1]
  if (l:pos[1] == s:indices[0])
    let prevPos = l:lastFieldLine
  else
    for i in s:indices
      if (i >= l:pos[1])
        break
      else
        let prevPos = i
      endif
    endfor
  endif
  return l:prevPos
endfunction

function NextField()
  let next = GetNextFieldIndex()
  exe next
endfunction

function PrevField()
  let prev = GetPrevFieldIndex()
  exe prev
endfunction

function SetupTabNav()
  inoremap <buffer> <tab> <esc>:call NextField()<CR>i
  nnoremap <buffer> <tab> :call NextField()<CR>
  inoremap <buffer> <s-tab> <esc>:call PrevField()<CR>i
  nnoremap <buffer> <s-tab> :call PrevField()<CR>
endfunction

autocmd FileType spectre_panel call SetupTabNav()

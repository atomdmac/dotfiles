let g:vimwiki_list = [
      \ {'path': '~/Seafile/default/Documents/vimwiki/',
      \ 'syntax': 'markdown', 'ext': '.md'},
      \ {'path': '~/Seafile/default/Documents/rpg-wiki/',
      \ 'syntax': 'markdown', 'ext': '.md'}
      \]
let g:vimwiki_folding = 'custom'
let g:vimwiki_global_ext = 1

" Disable some vimwiki keybinds that clash with my other keybinds.
let g:vimwiki_key_mappings = { 'lists': 0, }

" General/Global keybinds
nmap <leader>wl :VimwikiUISelect<CR>

function SetVimWikiKeybinds ()
  nmap <buffer> <leader>wt :VimwikiToggleListItem<CR>
  setlocal syntax=markdown

  " Prevent vimwiki from stealing <CR> when auto-complete pop-up is open.
  inoremap <silent><expr><buffer> <cr> pumvisible() ? coc#_select_confirm()
        \: "<C-]><ESC>:VimwikiReturn 1 1<CR>"
endfunction

autocmd FileType vimwiki call SetVimWikiKeybinds()

" For vim-markdown-folding
function SetMarkdownFolding()
  setlocal foldexpr=NestedMarkdownFolds()
  setlocal foldmethod=expr
endfunction

autocmd Syntax markdown call SetMarkdownFolding()

" For plasticboy/vim-markdown
" autocmd Syntax markdown set foldexpr=Foldexpr_markdown(v:lnum)

# Add ASDF plugins.
echo "Adding ASDF plugins..."

# Runtimes / compilers
asdf plugin add nodejs && asdf install nodejs latest
asdf plugin add ruby && asdf install ruby latest
asdf plugin add python && asdf install python latest
asdf plugin add golang && asdf install golang latest
asdf plugin add rust && asdf install rust latest

# Tools.
asdf plugin add neovim && asdf install neovim latest
asdf plugin add fzf && asdf install fzf latest
asdf plugin add stylua && asdf install stylua latest
asdf plugin add lua-language-service && asdf install lua-language-service latest
asdf plugin add ripgrep && asdf install ripgrep latest
asdf plugin add zoxide && asdf install zoxide latest
asdf plugin add shfmt && asdf install shfmt latest

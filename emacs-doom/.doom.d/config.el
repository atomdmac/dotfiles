;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Adam Macumber"
      user-mail-address "adam.macumber@sharpnotions.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative' .
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; EVIL Mode is Best Mode
(evil-mode t)
(define-key evil-normal-state-map (kbd "<right>") 'evil-shift-right)
(define-key evil-normal-state-map (kbd "<left>") 'evil-shift-left)
(define-key evil-visual-state-map (kbd "<right>") 'evil-shift-right)
(define-key evil-visual-state-map (kbd "<left>") 'evil-shift-left)
(define-key evil-normal-state-map (kbd "s") 'evilem-motion-find-char)
(define-key evil-normal-state-map (kbd "S") 'evilem-motion-find-char-backward)
;; (define-key evil-normal-state-map (kbd "z a") '+fold/toggle)

;; Drop me where I was last time
(desktop-save-mode 1)

;; Enable Prettier
(add-hook 'web-mode-hook 'prettier-js-mode)

;; Easily open a terminal
(map! :leader "o z" 'term)

;; Launch Ranger
;; (map! :leader "f t" 'ranger)

;; Customize Find File Keys
(map! :leader "f f" '+default/find-file-under-here)
(map! :leader "f F" 'counsel-find-file)

;; Customize Window Keys
(map! :leader "w s" 'evil-split-buffer)
(map! :leader "w v" 'evil-window-vsplit)

;; Customize Magit Keys
(map! :leader "g p" 'magit-pull)
(map! :leader "g P" 'magit-push)

;; Ivy
;; Enable fuzzy match
(setq ivy-re-builders-alist '((t . ivy--regex-fuzzy)))

;; Comment
(map! :leader "t c" 'comment-line)

;; Themes
(load-theme 'badwolf t)

;; Enable Prettier for TypeScript
(add-hook 'typescript-mode 'prettier-js-mode)

;; Enable Typescript Stuff for Web-Mode
;; (add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
;; (add-hook 'web-mode-hook
;;          (lambda ()
;;            (when (string-equal "tsx" (file-name-extension buffer-file-name))
;;              (setup-tide-mode))))
;; ;; enable typescript-tslint checker
;; (flycheck-add-mode 'typescript-tslint 'web-mode)

;; (add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
;; (add-hook 'typescript-mode-hook)

#!/bin/bash

LOG_FILE=~/.dunst.log
MAX_LOG_LENGTH=100

# Create log file if it doesn't already exist
if [! -f $LOG_FILE ]
then
  echo "Begin Notification Log" > $LOG_FILE
fi

# Name input parameters for easy reading.
APPNAME=$1
SUMMARY=$2
BODY=$3
ICON=$4
URGENCY=$5
DATE=$(date -Iseconds)

# Prepend new entry to the log
OUTPUT="$DATE,$APPNAME,\"$SUMMARY\",\"$BODY\",$ICON,$URGENCY"
sed -i "1s;^;$OUTPUT\n;" $LOG_FILE

# Limit log history
EXISTING_LOG=$(head -n$MAX_LOG_LENGTH $LOG_FILE)
echo "$EXISTING_LOG" > $LOG_FILE

# Fonts
font_size 14.0

# Gimme ligatures
disable_ligatures never

# Tab Position
tab_bar_edge top
tab_bar_style         custom
tab_separator         ""
tab_title_template "( {fmt.bold}{index}{fmt.nobold} ) {f'{title[:6]}…{title[-6:]}' if title.rindex(title[-1]) + 1 > 13 else title} "
tab_bar_margin_width 5
tab_bar_margin_height 5 5

active_tab_foreground   #000
active_tab_background   #fff
active_tab_font_style   bold
inactive_tab_foreground #444
inactive_tab_background #777
inactive_tab_font_style normal

## Layouts
enabled_layouts splits

# Please stop closing my windows...
confirm_os_window_close 1

# Give me lots of screen_scrollback
scrollback_lines 4000

# Bells
enable_audio_bell     no
visual_bell_duration  0.25

# Window Style
window_border_width   0.5
active_border_color #fff
inactive_border_color #000000
window_margin_width  2
# background            #333333
# background_opacity    1

# Pull in ENV-specific configuration.
include darwin.conf

# map cmd+` nth_os_window +1

# For some reason includes don't work on MacOS.
include dracula.conf

# https://draculatheme.com/kitty
#
# Installation instructions:
#
#  cp dracula.conf ~/.config/kitty/
#  echo "include dracula.conf" >> ~/.config/kitty/kitty.conf
#
# Then reload kitty for the config to take affect.
# Alternatively copy paste below directly into kitty.conf

foreground            #f8f8f2
background            #282a36
selection_foreground  #ffffff
selection_background  #44475a

url_color #8be9fd

# black
color0  #21222c
color8  #6272a4

# red
color1  #ff5555
color9  #ff6e6e

# green
color2  #50fa7b
color10 #69ff94

# yellow
color3  #f1fa8c
color11 #ffffa5

# blue
color4  #bd93f9
color12 #d6acff

# magenta
color5  #ff79c6
color13 #ff92df

# cyan
color6  #8be9fd
color14 #a4ffff

# white
color7  #f8f8f2
color15 #ffffff

# Cursor colors
cursor            #f8f8f2
cursor_text_color background

# Tab bar colors
active_tab_foreground   #282a36
active_tab_background   #f8f8f2
inactive_tab_foreground #282a36
inactive_tab_background #6272a4

# Marks
mark1_foreground #282a36
mark1_background #ff5555

# Splits/Windows
active_border_color #f8f8f2
inactive_border_color #6272a4

# Diff colors
foreground           #f8f8f2
background           #282a36
title_fg             #f8f8f2
title_bg             #282a36
margin_bg            #6272a4
margin_fg            #44475a
removed_bg           #ff5555
highlight_removed_bg #ff5555
removed_margin_bg    #ff5555
added_bg             #50fa7b
highlight_added_bg   #50fa7b
added_margin_bg      #50fa7b
filler_bg            #44475a
hunk_margin_bg       #44475a
hunk_bg              #bd93f9
search_bg            #8be9fd
search_fg            #282a36
select_bg            #f1fa8c
select_fg            #282a36

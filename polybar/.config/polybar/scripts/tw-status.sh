#!/bin/bash

get_tags() {
  TAG=1
  TAG_TOTAL=`timew get dom.active.tag.count`
  OUTPUT=""

  if [ $TAG_TOTAL -eq 0 ];
  then
    return
  fi

  echo "$TAG_TOTAL total tags"
  until [ $TAG -gt $TAG_TOTAL ]
  do
    OUTPUT+=`timew get dom.active.tag.$TAG`
    TAG=`expr $TAG + 1`
  done
  return $OUTPUT
}

STATUS=$(timew get dom.active)
if [ $STATUS -gt 0 ];
then
  RESULT=`timew get dom.active.duration | sed s/PT// | sed s/M/:/ | sed s/S//`
  echo " $RESULT $TAGS"
else
  echo " Relaxing..."
fi

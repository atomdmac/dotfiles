#!/bin/bash

# ASDF Version Manager Initialization
# ***
# NOTE: This is not a part of ASDF!  It is an initi script from dotfiles!
# ***
ASDF_ROOT="$HOME/.asdf"
ASDF_PATH="$ASDF_ROOT/asdf.sh"
if [ -f $ASDF_PATH ]; then
  if ! [ -x "$ASDF_PATH" ]; then
    echo "ASDF installed by not executable... Attempting to fix..."
    chmod u+x $ASDF_PATH
    if ! [ -x "$ASDF_PATH" ]; then
      echo "Unable for fix permissions for $ASDF_PATH"
    else
      echo "ASDF permissions fixed successfully!"
    fi
  fi
  source "$ASDF_PATH"

  # Add additional ASDF-managed binaries to the path.
  GOLANG_BIN_DIR="$(asdf where golang)/packages/bin"
  if [ -d "$GOLANG_BIN_DIR" ]; then
    export PATH="$GOLANG_BIN_DIR:$PATH"
  fi
else
  echo "ASDF not installed.  Skipping..."
fi

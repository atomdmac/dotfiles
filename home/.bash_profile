source ~/.bashrc

# For gogo tab completion
eval "$(PATH="$(pyenv prefix 3.8.7)/bin:$PATH" register-python-argcomplete gogo)"

# Rust / Cargo environment
. "$HOME/.cargo/env"

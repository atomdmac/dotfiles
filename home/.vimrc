" Syntax highlighting + formatting
syntax on
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smarttab
filetype plugin on
set nocompatible

" Line wrapping
set wrap
set linebreak
set showbreak=---->\  " Indicator for soft-wrap line breaks
set textwidth=0
set wrapmargin=0

" Folding
set foldmethod=indent
set foldnestmax=10
" Automatically collapse folds when file is opened.
set foldlevelstart=2
function GetFoldText()
  return substitute(getline(v:foldstart), '\\\\t', repeat('\\ ', &tabstop),'g').'...'.trim(getline(v:foldend)) . ' (' . (v:foldend - v:foldstart + 1) . ' lines)'
endfunction
set foldtext=GetFoldText()

" Show whitespace
set list

" Show line at column 80
set colorcolumn=80,100
highlight ColorColumn ctermbg=darkgrey guibg=Grey3

" Highlight current line
set cursorline

" Show line numbers
set number

" Change file saving strategy
" Fixes issues w/ webpack hotreloading
set backupcopy=yes

" Sync Vim clipboard to system clipboard
if has('unix')
  " The `+` register is used by X-server
  set clipboard=unnamedplus
else
  set clipboard=unnamed
endif

" Mouse support
if has('mouse')
  set mouse=a
endif

" Show title in terminal
set title
set titleold="Terminal"
set titlestring=%F

" Allow :find to fuzzy-find files in `pwd`
set path=.,**

" Leave `x` lines when scrolling
set scrolloff=5

" -----------------------------------------------------------------------------
" Text Editing
" -----------------------------------------------------------------------------

" Move lines up/down
nnoremap <DOWN> :m .+1<CR>==
vnoremap <DOWN> :m '>+1<CR>gv=gv
nnoremap <UP> :m .-2<CR>==
vnoremap <UP> :m '<-2<CR>gv=gv

" Move word-wise with ALT key.
imap <A-LEFT> <ESC>bi
imap <A-RIGHT> <ESC>eli
imap <A-BACKSPACE> <C-w>

" (In|De)dent lines with <LEFT>/<RIGHT>
nnoremap <S-TAB> <<
nnoremap <TAB> >>
vnoremap <S-TAB> <gv
vnoremap <TAB> >gv

" -----------------------------------------------------------------------------
" Search <space>s
" -----------------------------------------------------------------------------

" Engage case-insensitive search!
set ignorecase

" Make sure search highlight is prominant.
highlight Search ctermbg=yellow ctermfg=black guibg=yellow guifg=black

" Center cursor after search
nnoremap n nzz
nnoremap N Nzz

" Search for selected text
vnoremap <space>sf "hy/<c-r>h<CR>
vnoremap / "hy/<c-r>h<CR>
" Select word if no text is selected
nmap <space>sf viw<space>sf

" Search and replace selected text
vnoremap <space>sr "hy:%s/<C-r>h//gc<left><left><left>

" Clear highlighting on escape in normal mode
nnoremap <silent> <esc> :noh<return><esc>
nnoremap <silent> <esc>^[ <esc>^[

" Use RipGrep for :grep commands
if executable("rg")
  set grepprg=rg
endif

" Search for `search` within `path` and send search results to quickfix list.
function SearchFor(search, path)
  if &grepprg == 'rg'
    let grep_options = '--vimgrep --hidden'
  else
    let grep_options = '-r'
  endif
  :execute 'silent grep! ' .. l:grep_options .. ' ' .. a:search .. ' ' .. a:path | copen
endfunction

" Request a search term input from the user and send results to quickfix list.
function SearchForInput(path)
  let search = input("Find: ")
  " Bail if the query is empty / user hits ESC to cancel.
  if search == ''
    return
  endif
  call SearchFor(l:search, a:path)
endfunction

" Search directory of current file for search term.
nmap <silent> <leader>sp :call SearchForInput(expand('%:p:h'))<CR>
" Search project for search term.
nmap <silent> <leader>sP :call SearchForInput(trim(execute('pwd')))<CR>

" Search directory of current file for TODO items.
nmap <silent> <space>st :call SearchFor('TODO:', expand('%:p:h'))<CR>
" Search project for TODO items.
nmap <silent> <space>sT :call SearchFor('TODO:', trim(execute('pwd')))<CR>

" Find and replace in buffer
vmap <c-/> y:<c-u>%s/<c-r>"//g<left><left>

" Show current file name
nmap <leader>fn :echo "Current File: " . expand("%:p")<CR>

" -----------------------------------------------------------------------------
" Buffer
" -----------------------------------------------------------------------------

" Quicksave
nmap <space>ww :w<CR>
nmap <space>xx :x<CR>

" Alternate: Next / Previous Buffer
nmap <LEFT> :bp<CR>
nmap <RIGHT> :bn<CR>

" Close current buffer without losing split
" (Why this has to be an 'add-on' is beyond me... 😠)
nmap <silent> <space>bd :bp\|bd #<CR>
nmap <silent> <space>BD :bufdo :bp\|bd #<CR>

" Re-open recently closed buffer
nmap <space>bo <c-o><CR>

" Don't show QuickFix buffer when cycling buffers
augroup qf
    autocmd!
    autocmd FileType qf set nobuflisted
augroup END

" Source the current files
nnoremap <a-r> :so %<cr>:echo "Loaded " . expand("%:p")<cr>

" -----------------------------------------------------------------------------
" Jumps
" -----------------------------------------------------------------------------
nnoremap <c-j> <c-i>
nnoremap <c-k> <c-o>

" -----------------------------------------------------------------------------
" Windows / Splits <space>w
" -----------------------------------------------------------------------------

" Close windows
nmap <space>wd :q<CR>

" Open splits
nmap <space>ws :sp<CR>
nmap <space>wv :vs<CR>

" Resize splits
nmap <space>w= <c-w>=

nmap <A-LEFT> <c-w><
tmap <A-LEFT> <esc><c-w><

nmap <A-RIGHT> <c-w>>
tmap <A-RIGHT> <esc><c-w>>

nmap <A-UP> <c-w>+
tmap <A-UP> <esc><c-w>+

nmap <A-DOWN> <c-w>-
tmap <A-DOWN> <esc><c-w>-


" Move between splits
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Move between splits (alternative method)
nnoremap <space>wh <c-w>h
nnoremap <space>wl <c-w>l
nnoremap <space>wj <c-w>j
nnoremap <space>wk <c-w>k

" Quit vim
nmap <space>qq :qa<CR>

" -----------------------------------------------------------------------------
" Terminals
" -----------------------------------------------------------------------------

" Terminal Emulator
function OpenTerminal(side) abort
  " Open a new split if applicable.
  if a:side == 'bottom'
    :sp
    :wincmd j
  elseif a:side == 'right'
    :vs
    :wincmd l
  endif

  :terminal
endfunction

function PrepTermBuffer() abort
  " Don't list the buffer.
  set nobuflisted

  " Set some keymaps for standard shells (but not stuff like FZF, etc.)
  if match(bufname('%'), 'zsh') >= 0
    " Allow ESC to exit "insert" mode in terminal buffer
    tnoremap <buffer> <Esc> <C-\><C-n>

    " Disable quick nav between buffers.
    nmap <buffer> <LEFT> :echo "Changing buffers in terminals is disabled."<CR>
    nmap <buffer> <RIGHT> :echo "Changing buffers in terminals is disabled."<CR>
  endif
endfunction

augroup termIgnore
  autocmd!
  if has('nvim')
    autocmd TermOpen * call PrepTermBuffer()
  else
    autocmd TerminalOpen * call PrepTermBuffer()
  end
augroup END

" Open terminal windows.
nmap <a-t> :call OpenTerminal('right')<CR>

" -----------------------------------------------------------------------------
" Spelling <leader>s
" -----------------------------------------------------------------------------

" Toggle on/off
nmap <leader>st :set spell!<CR>
" Suggest replacment
nmap <leader>ss z=

" -----------------------------------------------------------------------------
" File Exploration <space>e
" -----------------------------------------------------------------------------

" Open netrw at file directory (i.e. "here")
nmap <space>eh :Vex<CR>
" Open netrw at project directory
nmap <space>ep :exe 'Vex' getcwd()<CR>

let g:netrw_banner = 0 " Hide the help banner
let g:netrw_liststyle = 3 " Long listing (file file per line w/ timestamp & size)
let g:netrw_browse_split = 4 " Open files in preview window
let g:netrw_winsize = 20 " Initial width of the window
let g:netrw_hide = 0 " Show hidden files
let g:netrw_keepdir = 0 " Commands act on the current visible directory

" Custom keybinds for NetRW buffers.
function! SetNetRwKeybinds()
  nmap <buffer> za <CR>
endfunction
au! FileType netrw call SetNetRwKeybinds()

" -----------------------------------------------------------------------------
" File Type Associations
" -----------------------------------------------------------------------------

autocmd BufNewFile,BufRead *.svelte set syntax=html

" -----------------------------------------------------------------------------
" External Tools
" -----------------------------------------------------------------------------

" Attempt to use Python to pretty-print JSON
nnoremap <leader>j :%!python -m json.tool<CR>

" -----------------------------------------------------------------------------
" Undo
" -----------------------------------------------------------------------------
if has('persistent_undo')         "check if your vim version supports
  set undodir=$HOME/.vim/undo     "directory where the undo files will be stored
  set undofile                    "turn on the feature
endif

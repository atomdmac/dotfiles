#!/bin/bash

if [[ -x $(which exa) ]]; then
  LS_BIN='exa'
else
  LS_BIN='ls'
fi

if [[ -x $(which podman) ]]; then
  DOCKER_BIN='podman'
else
  DOCKER_BIN='docker'
fi

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias la="$LS_BIN -alF"
alias ll="$LS_BIN -al"
alias sdkr="sudo docker"
alias pod="podman"
alias pc="podman-compose"
alias clock="while true; do clear; date; sleep 1; done"

# Docker / Compose Helpers
dockerComposeFacade() {
  case "$1" in
    # Custom Commands
    shell)
      shift; dockerComposeShell "$@";;

    # Forward all other commands directly to docker-compose
    *)
      $DOCKER_BIN compose "$@"
  esac
}

dockerComposeShell() {
  SHELL_PATH="${2:-/bin/bash}"
  docker-compose exec "$1" "$SHELL_PATH"
}

# alias docker="$DOCKER_BIN"
alias d="docker"
alias dc="dockerComposeFacade"

# Git Helpers
alias g="git"
alias gd="git diff"
alias gl="git pull"
alias gp="git push"
alias gco="git checkout"

getAuthorsSince() {
  if [[ -z "$1" ]]; then
    echo "Usage: gsince <commit-ish>"
    return
  fi
  echo "Authors since commit '$1':"
  git log "$1".. --format="%aN" --reverse | sort -u
}
alias gsince="getAuthorsSince"
alias git-since="getAuthorsSince"

# List docker network subnet masks
listDockerSubnets() {
for i in $(docker network ls --format "{{ .Name }}" --filter driver=bridge); do docker network inspect -f "{{ .Name }} -  {{ (index .IPAM.Config 0).Subnet }}" "$i"; done
}
alias dnets="listDockerSubnets"

function stopAllDockerContainers () {
  docker stop $(docker ps -q)
}
alias dkill="stopAllDockerContainers"

# Quick Search
quickFolderSearch() {
  grep -rn --ignore-case --exclude-dir={node_modules,test} "$1" "${2:-./}"
}
alias grr="quickFolderSearch"

# Find + Replace In Multiple Files
findAndReplaceMultiple() {
  toFind=$1
  toReplace=$2
  echo "Replacing \"$toFind\" with \"$toReplace\".  Continue? (Y to confirm, anything else to cancel)"
  while true; do
    read -r yn
    case $yn in
      [Y]* ) sed -i '.farmbak' "s/$toFind/$toReplace/g" "$(find . -type f)"; break;;
      * ) echo 'Find and Replace operation canceled.'; break;;
    esac
  done
}
alias farm="findAndReplaceMultiple"

# Convert to Base64
removeWhitespace() {
  echo -n "${1//[$'\t\r\n ']}"
}
toBase64() {
  RAW=$1
  echo "RAW:$RAW"
  if [[ -z "$RAW" ]]; then
    while read -r line; do
      echo "whillll"
      RAW="$RAW$line"
    done
  fi
  echo "Converting \"$RAW\" to Base64..."
  ENCODED=$(echo -n "$RAW" | base64)
  echo -n "${ENCODED//[$'\t\r\n ']}" | pbcopy
}
alias b64="toBase64"
fromBase64() {
  DECODED=$(echo -n "$1" | base64 --decode)
  echo -n "${DECODED//[$'\t\r\n ']}"
}
alias b64d="fromBase64"

# TimeWarrior Helpers
alias tws="timew summary :ids"
alias twsw="timew summary :ids :week"
alias twsy="timew summary :ids :yesterday"
alias twc="timew get dom.active.duration"

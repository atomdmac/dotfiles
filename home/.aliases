#!/bin/bash
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias la="ls -alF"
alias sdkr="sudo docker"
alias pod="podman"
alias clock="while true; do clear; date; sleep 1; done"

# Docker / Compose Helpers
dockerComposeFacade() {
  case "$1" in
    # Custom Commands
    shell)
      shift; dockerComposeShell "$@";;

    # Forward all other commands directly to docker-compose
    *)
      docker compose "$@"
  esac
}

dockerComposeShell() {
  SHELL_PATH="${2:-/bin/bash}"
  docker-compose exec "$1" "$SHELL_PATH"
}

alias d="docker"
alias dc="dockerComposeFacade"

# Git Helpers
alias g="git"
alias gd="git diff"
alias gl="git pull"
alias gp="git push"
alias gco="git checkout"

getAuthorsSince() {
  if [[ -z "$1" ]]; then
    echo "Usage: gsince <commit-ish>"
    return
  fi
  echo "Authors since commit '$1':"
  git log "$1".. --format="%aN" --reverse | sort -u
}
alias gsince="getAuthorsSince"

# List docker network subnet masks
listDockerSubnets() {
for i in $(docker network ls --format "{{ .Name }}" --filter driver=bridge); do docker network inspect -f "{{ .Name }} -  {{ (index .IPAM.Config 0).Subnet }}" "$i"; done
}
alias dnets="listDockerSubnets"

function stopAllDockerContainers () {
  docker stop $(docker ps -q)
}
alias dkill="stopAllDockerContainers"

# Quick Search
quickFolderSearch() {
  grep -rn --ignore-case --exclude-dir={node_modules,test} "$1" "${2:-./}"
}
alias grr="quickFolderSearch"

# Find + Replace In Multiple Files
findAndReplaceMultiple() {
  toFind=$1
  toReplace=$2
  echo "Replacing \"$toFind\" with \"$toReplace\".  Continue? (Y to confirm, anything else to cancel)"
  while true; do
    read -r yn
    case $yn in
      [Y]* ) sed -i '.farmbak' "s/$toFind/$toReplace/g" "$(find . -type f)"; break;;
      * ) echo 'Find and Replace operation canceled.'; break;;
    esac
  done

  # 
}
alias farm="findAndReplaceMultiple"

# TimeWarrior Helpers
alias tws="timew summary :ids"
alias twsw="timew summary :ids :week"
alias twsy="timew summary :ids :yesterday"
alias twc="timew get dom.active.duration"

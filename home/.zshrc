export ZPLUG_HOME=~/.zplug
export ZPLUG_MANUAL_LOADING=YEP

# Define plugins to install/load here.
#
# If ZPLUG_MANUAL_LOADING is enabled, make sure to add an entry to the
# load_plugins_manually function below as well.
function load_plugins_with_zplug {
  zplug "dracula/zsh", as:theme
  zplug "plugins/z", from:oh-my-zsh
  zplug "zsh-users/zsh-autosuggestions"
}

# Load plugins without relying on ZPlug when we can since loading ZPlug first
# takes awhile and I hate waiting.
function load_plugins_manually {
  source ~/.zplug/repos/dracula/zsh/dracula.zsh-theme
  source ~/.zplug/repos/robbyrussell/oh-my-zsh/plugins/z/z.plugin.zsh
  source ~/.zplug/repos/zsh-users/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
}

# Load ZPlug and plugins.
function load_zplug {
  source ~/.zplug/init.zsh
  load_plugins_with_zplug
}

# Load plugins using either ZPlug or by including plugin source directly.
function load_plugins {
  # Load installed plugins so they can be used.
  if [ -v ZPLUG_MANUAL_LOADING ]
  then
    load_plugins_manually
  else
    load_zplug
    zplug load
  fi
}

# Install ZPlug if it's not installed.
if [ ! -d "$ZPLUG_HOME" ]
then
  # Install ZPlug
  echo "ZPlug not installed.  Attempting to install it now..."
  git clone https://github.com/zplug/zplug $ZPLUG_HOME

  # Tell ZPlug about the plugins we'd like to install.
  echo "Installing plugins from .zshrc"
  load_zplug
  zplug install

  # Put new changes into effect immediately!
  load_plugins
else
  load_plugins
  # Loading ZPlug slows down our load time.  Only include it when we need it.
  # TODO: Pass thru arguments on first call.
  alias zplug="unalias 'zplug' && load_zplug && echo 'ZPlug Loaded!' && zplug $@"
fi

# Disable vi-mode (conflicts with TMUX)
bindkey -e

# Load common shell settings
source ~/.shellrc

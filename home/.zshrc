export ZPLUG_HOME=~/.zplug
export ZPLUG_MANUAL_LOADING=YEP
export ZSH_THEME=dracula

autoload -U colors && colors
autoload -U compinit; compinit
setopt promptsubst

# Set prompt
autoload -Uz vcs_info # Load version control information
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '%b '
PROMPT='%F{blue}%~%f %F{red}${vcs_info_msg_0_}%f$ '

# Define plugins to install/load here.
#
# If ZPLUG_MANUAL_LOADING is enabled, make sure to add an entry to the
# load_plugins_manually function below as well.
function load_plugins_with_zplug {
  zplug "zsh-users/zsh-autosuggestions"
  zplug "Aloxaf/fzf-tab", use:"fzf-tab.plugin.zsh"
  zplug "catppuccin/zsh-syntax-highlighting"
}

# Load plugins without relying on ZPlug when we can since loading ZPlug first
# takes awhile and I hate waiting.
function load_plugins_manually {
  source ~/.zplug/repos/zsh-users/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
  source ~/.zplug/repos/Aloxaf/fzf-tab/fzf-tab.plugin.zsh
  source ~/.zplug/repos/catppuccin/zsh-syntax-highlighting/themes/catppuccin_mocha-zsh-syntax-highlighting.zsh
}

# Load ZPlug and plugins.
function load_zplug {
  source ~/.zplug/init.zsh
  load_plugins_with_zplug
}

# Load plugins using either ZPlug or by including plugin source directly.
function load_plugins {
  # Load installed plugins so they can be used.
  if [ $ZPLUG_MANUAL_LOADING = "YEP" ];
  then
    load_plugins_manually
  else
    load_zplug
    zplug load
  fi
}

# Install ZPlug if it's not installed.
if [ ! -d "$ZPLUG_HOME" ]
then
  # Install ZPlug
  echo "ZPlug not installed.  Attempting to install it now..."
  git clone https://github.com/zplug/zplug $ZPLUG_HOME

  # Tell ZPlug about the plugins we'd like to install.
  echo "Installing plugins from .zshrc"
  load_zplug
  zplug install

  # Put new changes into effect immediately!
  load_plugins
else
  load_plugins
  # Loading ZPlug slows down our load time.  Only include it when we need it.
  # TODO: Pass thru arguments on first call.
  alias zplug="unalias 'zplug' && load_zplug && echo 'ZPlug Loaded!' && zplug $@"
fi

# Disable vi-mode (conflicts with TMUX)
bindkey -e

# Load common shell settings
source ~/.shellrc

# EXPERIMENTAL: NNN Config
source ~/.nnn

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# Zoxide support
if [ -x $(which zoxide) ]; then
  eval "$(zoxide init zsh)"
else
  echo "zoxide not found.  Skipping initialization."
fi
